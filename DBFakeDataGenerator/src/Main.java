import java.io.*;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    //Database name must be specified:
    static String databaseName = "beertag";

    public static void main(String[] args) throws IOException {

        StringBuilder output = new StringBuilder();
        Random random = new Random();

        //Files used must be initialised here:
        File beersFile = new File("src/files/Beers");
        File countriesFile = new File("src/files/Countries");
        File namesFile = new File("src/files/Names");
        File lastNamesFile = new File("src/files/Lastnames");
        File breweriesFile = new File("src/files/Breweries");
        File tagsFile = new File("src/files/Tags");
        File stylesFile = new File("src/files/Styles");
        File abvFile = new File("src/files/abv");
        File dateTimeFile = new File("src/files/DateTime");

        //Lists
        List<String> firstNames, lastNames, beersName, breweries, styles, tags, countries, abv, dateTime, applicationRoles;
        firstNames = new ArrayList<>();
        lastNames = new ArrayList<>();
        beersName = new ArrayList<>();
        breweries = new ArrayList<>();
        styles = new ArrayList<>();
        tags = new ArrayList<>();
        countries = new ArrayList<>();
        abv = new ArrayList<>();
        dateTime = new ArrayList<>();
        applicationRoles = new ArrayList<>();

        List<List<String>> valueListsForColumns = new ArrayList<>();


        //Populating the lists
        fileReaderFunction(namesFile, firstNames);
        fileReaderFunction(lastNamesFile, lastNames);
        fileReaderFunction(beersFile, beersName);
        fileReaderFunction(countriesFile, countries);
        fileReaderFunction(breweriesFile, breweries);
        fileReaderFunction(tagsFile, tags);
        fileReaderFunction(stylesFile, styles);
        fileReaderFunction(abvFile, abv);
        fileReaderFunction(dateTimeFile, dateTime);

        //Generating insert queries

        //Countries table
        valueListsForColumns.add(countries);
        tableRowGenerator(false, random, 150, output, "countries", valueListsForColumns,
                "country_name");

        //Tags table
        valueListsForColumns.clear();
        valueListsForColumns.add(tags);
        valueListsForColumns.add(dateTime);
        tableRowGenerator(false, random, 75, output, "tags", valueListsForColumns,
                "tag_name", "date_created");

        //Breweries table
        valueListsForColumns.clear();
        valueListsForColumns.add(breweries);
        valueListsForColumns.add(dateTime);
        valueListsForColumns.add(idListGenerator(random, 150));
        valueListsForColumns.add(Stream.of("'/media/uploads/brewery-example.jpg'").collect(Collectors.toList()));
        valueListsForColumns.add(Stream.of("0").collect(Collectors.toList()));
        tableRowGenerator(true, random, 40, output, "breweries", valueListsForColumns,
                "brewery_name", "date_created", "country_id", "brewery_picture", "is_deleted");

        //Users table
        valueListsForColumns.clear();
        valueListsForColumns.add(firstNames);
        valueListsForColumns.add(lastNames);
        valueListsForColumns.add(emailListGenerator(random, 100, firstNames, lastNames));
        valueListsForColumns.add(passWordGenerator(100));
        valueListsForColumns.add(dateTime);
        applicationRoles.add("'User'");
        valueListsForColumns.add(applicationRoles);
        //id HAS TO BE VALID
        valueListsForColumns.add(idListGenerator(random, 100));
        tableRowGenerator(true, random, 100, output, "users", valueListsForColumns,
                "first_name", "last_name", "email", "user_password", "date_joined", "application_role", "country_id");

        //Styles table
        valueListsForColumns.clear();
        valueListsForColumns.add(styles);
        List<String> description = new ArrayList<>();
        description.add("'description'");
        valueListsForColumns.add(description);
        valueListsForColumns.add(Stream.of("0").collect(Collectors.toList()));
        valueListsForColumns.add(dateTime);
        tableRowGenerator(true, random, 95, output, "styles", valueListsForColumns,
                "style_name", "style_description", "is_deleted", "date_created");


        //Beers table
        valueListsForColumns.clear();
        valueListsForColumns.add(beersName);
        valueListsForColumns.add(Stream.of("'This is the description'").collect(Collectors.toList()));
        Collections.shuffle(abv);
        valueListsForColumns.add(abv);
        valueListsForColumns.add(dateTime);
        valueListsForColumns.add(idListGenerator(random, 30));
        valueListsForColumns.add(idListGenerator(random, 95));
        valueListsForColumns.add(Stream.of("'/media/uploads/beer-example.jpg'").collect(Collectors.toList()));
        valueListsForColumns.add(Stream.of("0").collect(Collectors.toList()));
        valueListsForColumns.add(idListGenerator(random, 10));
        valueListsForColumns.add(idListGenerator(random, 5));
        tableRowGenerator(true, random, 26, output, "beers", valueListsForColumns,
                "beer_name", "beer_description", "abv", "date_added", "brewery_id", "style_id", "beer_picture", "is_deleted", "average_rating", "user_id");

        valueListsForColumns.clear();
        valueListsForColumns.add(idListGenerator(random, 6));
        valueListsForColumns.add(idListGenerator(random, 26));
        valueListsForColumns.add(idListGenerator(random, 10));
        valueListsForColumns.add(Stream.of("'Evalarka'", "'Mnogo mi haresa'", "'Liubimata mi bira'", "'Top e'", "'Stava'", "'Ne mi haresa tolkova'", "'Ima i po-hubavi'").collect(Collectors.toList()));
        valueListsForColumns.add(dateTime);
        tableRowGenerator(true, random, 300, output, "ratings", valueListsForColumns,
                "user_id", "beer_id", "rating", "review", "date_created");

        System.out.println(output);
        SQLScriptWriter(output.toString());


//        How to find the current path:
//        Path currentRelativePath = Paths.get("");
//        String s = currentRelativePath.toAbsolutePath().toString();
//        System.out.println("Current relative path is: " + s);

    }

    private static void tableRowGenerator(boolean randomIndex, Random random, int rowCount, StringBuilder query, String tableName, List<List<String>> valueLists, String... columnNames) {
        String[] columns = new String[columnNames.length];
        String[] values = new String[valueLists.size()];

        System.arraycopy(columnNames, 0, columns, 0, columns.length);

        for (int i = 0; i < rowCount; i++) {
            for (int j = 0; j < columns.length; j++) {
                if (randomIndex) {
                    values[j] = valueLists.get(j).get(randomIndexPicker(random, valueLists.get(j)));
                } else {
                    values[j] = valueLists.get(j).get(i);
                }
            }
            query.append(insertQuery(tableName, columns, values));
        }

        query.append(System.lineSeparator()).append(System.lineSeparator());
    }

    private static int randomIndexPicker(Random random, List<String> list) {
        return random.nextInt(list.size());
    }

    private static String insertQuery(String tableName, String[] columns, String[] values) {

        return String.format("INSERT INTO %s.%s (", databaseName, tableName) + String.join(", ", columns) + ") VALUES (" +
                String.join(", ", values) + ");" + System.lineSeparator();
    }

    private static <T> void fileReaderFunction(File file, List<T> list) {
        try (BufferedReader bufferedReader = Files.newBufferedReader(file.toPath())) {
            T line;
            while ((line = (T) bufferedReader.readLine()) != null) {
                list.add(line);
            }
        } catch (IOException ex) {
            System.err.format("IOException: %s%n", ex);
        }
    }

    private static void SQLScriptWriter(String script) {
        File file = null;
        FileWriter fileWriter = null;
        BufferedWriter writer = null;

        try {
            file = new File("DBFakeData.sql");
            fileWriter = new FileWriter(file);
            // create file if not exists
            if (!file.exists()) {
                file.createNewFile();
            }
            // initialize BufferedWriter
            writer = new BufferedWriter(fileWriter);

            //write integers
            writer.write(script);
            System.out.println("File written successfully.");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // close BufferedWriter
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            // close FileWriter
            if (fileWriter != null) {
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static List<String> idListGenerator(Random random, int bound) {
        List<String> indexes = new ArrayList<>();
        for (int i = 1; i < bound; i++) {
            indexes.add(String.valueOf((random.nextInt(bound)) + 1));
        }
        return indexes;
    }

    private static List<String> emailListGenerator(Random random, int numberOfEmails, List<String> names, List<String> lastNames) {
        List<String> emails = new ArrayList<>();

        for (int i = 0; i < numberOfEmails; i++) {
            emails.add(String.format("'%s.%s@gmail.com'",
                    names.get(random.nextInt(names.size())).replace("'", ""),
                    lastNames.get(random.nextInt(lastNames.size())).replace("'", "")));
        }

        return emails;
    }

    private static List<String> passWordGenerator(int numberOfPasswords) {
        List<String> passwords = new ArrayList<>();
        for (int i = 0; i < numberOfPasswords; i++) {
            passwords.add(String.format("'%s'", UUID.randomUUID().toString()));
        }

        return passwords;
    }
}
