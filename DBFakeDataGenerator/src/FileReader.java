import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class FileReader {
    private BufferedReader fileReader;

    public FileReader(String filePath) throws IOException {
        File file = new File(filePath);
        fileReader = new BufferedReader(new java.io.FileReader(file));
    }




}
