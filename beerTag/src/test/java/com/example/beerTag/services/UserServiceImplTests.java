package com.example.beerTag.services;

        import com.example.beerTag.Factory;
        import com.example.beerTag.entities.Beer;
        import com.example.beerTag.entities.Brewery;
        import com.example.beerTag.entities.Style;

        import com.example.beerTag.entities.UserDetail;
        import com.example.beerTag.exceptions.DuplicateEntityException;
        import com.example.beerTag.exceptions.EntityNotFoundException;
        import com.example.beerTag.repositories.UsersRepository;
        import org.junit.Assert;
        import org.junit.Test;
        import org.junit.jupiter.api.Assertions;
        import org.junit.runner.RunWith;
        import org.mockito.InjectMocks;
        import org.mockito.Mock;
        import org.mockito.Mockito;
        import org.mockito.junit.MockitoJUnitRunner;

        import java.util.Arrays;
        import java.util.List;

        import static org.mockito.BDDMockito.willThrow;
        import static org.mockito.Mockito.doThrow;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests {

    private List<UserDetail> users;
    private UserDetail testUser = new UserDetail();
    private Beer testBeer = new Beer();
    private final int testId = 1;


    @Mock
    UsersRepository repository;

    @InjectMocks
    UserServiceImpl userService;


    @Test
    public void deleteUserShould_DeleteUser_whenUserExist() {
        //Arrange

        //Act
        userService.deleteUser(testId);
        //Assert
        Mockito.verify(repository, Mockito.times(1)).deleteUser(testId);
    }

    @Test
    public void getByIdShould_ReturnUser_WhenUserExists() {
        //Arrange
        UserDetail expectedUser = Factory.createUser();

        Mockito.when(repository.getById(testId)).thenReturn(expectedUser);
        //Act
        UserDetail returnedUser = userService.getById(testId);
        //Assert
        Assert.assertSame(expectedUser, returnedUser);
    }


    @Test
    public void getByEmailShould_ReturnUser_WhenUserExists() {
        //Arrange
        UserDetail expectedUser = Factory.createUser();
        expectedUser.setEmail("test");

        Mockito.when(repository.getByEmail("test")).thenReturn(expectedUser);
        //Act
        UserDetail returnedUser = userService.getByEmail("test");
        //Assert
        Assert.assertSame(expectedUser, returnedUser);
    }


    @Test
    public void getByIdShould_throwError_WhenUserNotExists() {
        //Arrange

        Mockito.when(repository.getById(testId)).thenThrow(EntityNotFoundException.class);
        //Act
        //Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> userService.getById(testId));
    }


    @Test
    public void createUserShould_ReturnUser_WhenUserNotExists() {
        //Act
        userService.createUser(testUser);
        //Assert
        Mockito.verify(repository, Mockito.times(1))
                .createUser(testUser);
    }

    @Test(expected=DuplicateEntityException.class )
    public void createUserShould_ThrowError_WhenUserAlreadyExists() {

        doThrow(new DuplicateEntityException())
                .when(repository)
                .createUser(testUser);

        userService.createUser(testUser);

    }

    @Test
    public void updateUserShould_ReturnUpdatedUser_WhenUserExists() {
        //Arrange
        UserDetail expectedUser = Factory.createUser();

        Mockito.when(repository.updateUser(expectedUser)).thenReturn(expectedUser);
        //Act
        UserDetail returnedUser = userService.updateUser(expectedUser);
        //Assert
        Assert.assertSame(expectedUser, returnedUser);
    }



    @Test
    public void getAllUsers_Should_ReturnAllUsers() {
        // Arrange
        Mockito.when(repository.getAll("","","")).thenReturn(Arrays.asList(
                new UserDetail("first", "last",
                        "user@saa.com"),
                new UserDetail("first1", "last1",
                        "user@saa.com1")
        ));
        // Act
        List<UserDetail> result = userService.getAll("","","");

        // Assert
        Assert.assertEquals(2, result.size());
    }

    @Test
    public void addBeerToDrank_should_callMethod() {
        // Arrange
        UserDetail user =Factory.createUser();
        Beer beer = new Beer();
        user.setEmail("test@abv.bg");
        beer.setId(1);

        int beerId = beer.getId();

        // Act
        userService.addBeerToDrankList(user, beerId);

        // Assert
        Mockito.verify(repository, Mockito.times(1))
                .addBeerToDrankList(user, beerId);
    }

    @Test(expected=DuplicateEntityException.class )
    public void addBeerToDrank_should_ThrowError_WhenBeerAlreadyExists() {

        doThrow(new DuplicateEntityException())
                .when(repository)
                .addBeerToDrankList(testUser,testId);

        userService.addBeerToDrankList(testUser,testId);
    }

    @Test(expected=EntityNotFoundException.class )
    public void addBeerToDrank_should_ThrowError_WhenBeerNotExists() {

        doThrow(new EntityNotFoundException("test"))
                .when(repository)
                .addBeerToDrankList(testUser,testId);

        userService.addBeerToDrankList(testUser,testId);
    }


    @Test
    public void addBeerToWish_should_callMethod() {
        // Arrange
        UserDetail user =Factory.createUser();
        Beer beer = new Beer();
        user.setEmail("test@abv.bg");
        beer.setId(1);

        int beerId = beer.getId();

        // Act
        userService.addBeerToWishList(user, beerId);

        // Assert
        Mockito.verify(repository, Mockito.times(1))
                .addBeerToWishList(user, beerId);
    }

    @Test(expected=DuplicateEntityException.class )
    public void addBeerToWish_should_ThrowError_WhenBeerAlreadyExists() {

        doThrow(new DuplicateEntityException())
                .when(repository)
                .addBeerToWishList(testUser,testId);

        userService.addBeerToWishList(testUser,testId);
    }

    @Test(expected=EntityNotFoundException.class )
    public void addBeerToWish_should_ThrowError_WhenBeerNotExists() {

        doThrow(new EntityNotFoundException("test"))
                .when(repository)
                .addBeerToWishList(testUser,testId);

        userService.addBeerToWishList(testUser,testId);
    }

    @Test
    public void deleteBeerFromList_should_callMethod() {
        // Arrange
        UserDetail user =Factory.createUser();
        Beer beer = new Beer();
        user.setEmail("test@abv.bg");
        beer.setId(1);

        int beerId = beer.getId();
        // Act
        userService.deleteBeerFromList(user, beerId);
        // Assert
        Mockito.verify(repository, Mockito.times(1))
                .deleteBeerFromList(user, beerId);
    }

    @Test(expected=EntityNotFoundException.class )
    public void deleteBeerFromList_should_ThrowError_WhenBeerNotExists() {

        doThrow(new EntityNotFoundException("test"))
                .when(repository)
                .deleteBeerFromList(testUser,testId);

        userService.deleteBeerFromList(testUser,testId);
    }




}
