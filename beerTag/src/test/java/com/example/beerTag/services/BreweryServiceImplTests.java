package com.example.beerTag.services;

import com.example.beerTag.entities.Brewery;
import com.example.beerTag.entities.Authority;

import com.example.beerTag.entities.UserDetail;
import com.example.beerTag.exceptions.DuplicateEntityException;
import com.example.beerTag.exceptions.EntityNotFoundException;
import com.example.beerTag.exceptions.InvalidOperationException;
import com.example.beerTag.repositories.BreweriesRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class BreweryServiceImplTests {
    private List<Brewery> breweries;

    @Mock
    BreweriesRepository repository;

    @InjectMocks
    BreweryServiceImpl service;

    @Before
    public void init() {
        Brewery brewery1 = new Brewery();
        Brewery brewery2 = new Brewery();
        Brewery brewery3 = new Brewery();
        brewery1.setId(1);
        brewery1.setName("brewery1");
        brewery2.setId(2);
        brewery2.setName("brewery2");
        brewery3.setId(3);
        brewery3.setName("brewery3");

        breweries = new ArrayList<>();
        breweries.add(brewery1);
        breweries.add(brewery2);
        breweries.add(brewery3);
    }

    @Test
    public void getAll_Should_ReturnAllBreweries() {
        //Arrange
        Mockito.when(repository.getAll("", ""))
                .thenReturn(breweries);
        //Act
        List<Brewery> returnedBreweries = service.getAll("", "");

        //Assert
        Assert.assertEquals(breweries, returnedBreweries);
        Assert.assertEquals(breweries.size(), returnedBreweries.size());
    }

    @Test
    public void getById_Should_ReturnBrewery_WhenBreweryExists() {
        //Arrange
        Mockito.when(repository.getById(1))
                .thenReturn(breweries.get(0));
        //Act
        Brewery returnedBrewery = service.getById(1);

        //Assert
        Assert.assertEquals(breweries.get(0), returnedBrewery);
    }

    @Test
    public void getById_Should_ThrowException_WhenBreweryDoesNotExist() {
        //Arrange
        Mockito.when(repository.getById(1))
                .thenThrow(EntityNotFoundException.class);
        //Act
        //Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.getById(1));
    }

    @Test
    public void createBrewery_Should_ReturnBrewery_WhenItDoesNotExist_AndUserHasAuthority() {
        //Arrange
        Mockito.when(repository.createBrewery(breweries.get(0)))
                .thenReturn(breweries.get(0));
        //Act
        Brewery returnedBrewery = service.createBrewery(breweries.get(0));

        //Assert
        Assert.assertEquals(breweries.get(0), returnedBrewery);
    }

    @Test
    public void createBrewery_Should_ThrowException_WhenBreweryExists() {
        //Arrange
        Mockito.when(repository.createBrewery(breweries.get(0)))
                .thenThrow(DuplicateEntityException.class);
        //Act
        //Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.createBrewery(breweries.get(0)));
    }

    @Test
    public void updateBrewery_Should_ReturnBrewery_WhenItExist_AndUserHasAuthority() {
        //Arrange
        Mockito.when(repository.updateBrewery(breweries.get(0).getId(), breweries.get(0)))
                .thenReturn(breweries.get(0));
        //Act
        Brewery returnedBrewery = service.updateBrewery(breweries.get(0).getId(), breweries.get(0));

        //Assert
        Assert.assertEquals(breweries.get(0), returnedBrewery);
    }

    @Test
    public void updateBrewery_Should_ThrowException_WhenBreweryDoesNotExist() {
        //Arrange
        Mockito.when(repository.updateBrewery(breweries.get(0).getId(), breweries.get(0)))
                .thenThrow(EntityNotFoundException.class);
        //Act
        //Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.updateBrewery(breweries.get(0).getId(), breweries.get(0)));
    }

    @Test
    public void deleteBrewery_Should_deleteBrewery_WhenBreweryExists() {
        //Arrange

        //Act
        service.deleteBrewery(0);
        //Assert
        Mockito.verify(repository, Mockito.times(1)).deleteBrewery(0);
    }

}
