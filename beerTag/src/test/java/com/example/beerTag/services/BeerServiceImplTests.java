package com.example.beerTag.services;

import com.example.beerTag.entities.Beer;
import com.example.beerTag.entities.Brewery;
import com.example.beerTag.entities.Style;
import com.example.beerTag.exceptions.DuplicateEntityException;
import com.example.beerTag.exceptions.EntityNotFoundException;
import com.example.beerTag.repositories.BeerRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class BeerServiceImplTests {
    private List<Beer> beers;

    @Mock
    BeerRepository beerRepository;

    @InjectMocks
    BeerServicesImpl beerService;

    @Before
    public void init() {
        Beer beer1 = new Beer();

        beer1.setName("beer1");
        beer1.setId(1);

        Brewery brewery1 = new Brewery();
        brewery1.setName("brewery1");
        beer1.setBrewery(brewery1);

        Style style1 = new Style();
        style1.setName("style1");
        beer1.setStyle(style1);

        beers = new ArrayList<>();
        beers.add(beer1);
    }

    @Test
    public void getAll_Should_ReturnAllBeers() {
        //Arrange
        Mockito.when(beerRepository.getAll("", "", "", "",
                "", "", "", ""))
                .thenReturn(beers);

        //Act
        List<Beer> returnedBeers = beerService.getAll("", "", "",
                "", "", "", "", "");

        //Assert
        Assert.assertEquals(beers, returnedBeers);
    }

    @Test
    public void getById_Should_ReturnBeer_WhenBeerExists(){
        //Arrange
        Mockito.when(beerRepository.getBeerById(1))
                .thenReturn(beers.get(0));
        //Act
        Beer returnedBeer = beerService.getBeerById(1);

        //Assert
        Assert.assertEquals(beers.get(0), returnedBeer);

    }

    @Test
    public void getById_Should_ThrowException_whenBeerDoesntExist(){
        //Arrange
        Mockito.when(beerRepository.getBeerById(10)).thenThrow(EntityNotFoundException.class);

        //Act
        //Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> beerService.getBeerById(10));
    }

    @Test
    public void createBeer_Should_ReturnBeer_WhenItDoesntExist(){
        //Arrange
        Mockito.when(beerRepository.createBeer(beers.get(0)))
                .thenReturn(beers.get(0));
        //Act
        Beer returnedBeer = beerService.createBeer(beers.get(0));

        //Assert
        Assert.assertEquals(beers.get(0), returnedBeer);
    }

    @Test
    public void createBeer_Should_ThrowException_WhenBeerExists(){
        //Arrange
        Mockito.when(beerRepository.createBeer(beers.get(0))).thenThrow(DuplicateEntityException.class);

        //Act
        //Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> beerService.createBeer(beers.get(0)));
    }

    @Test
    public void updateBeer_Should_ReturnBeer_WhenItExist(){
        //Arrange
        //Act
        beerService.updateBeer(beers.get(0));

        //Assert
        Mockito.verify(beerRepository, Mockito.times(1)).updateBeer(beers.get(0));
    }

    @Test
    public void deleteBeer_Should_deleteBeer_WhenBreweryExists() {
        //Arrange

        //Act
        beerService.deleteBeer(0);
        //Assert
        Mockito.verify(beerRepository, Mockito.times(1)).deleteBeer(0);
    }
}
