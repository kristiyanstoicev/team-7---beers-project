package com.example.beerTag.services;

import com.example.beerTag.entities.Beer;
import com.example.beerTag.entities.Rating;
import com.example.beerTag.entities.UserDetail;
import com.example.beerTag.exceptions.EntityNotFoundException;
import com.example.beerTag.exceptions.InvalidOperationException;
import com.example.beerTag.repositories.RatingRepository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(MockitoJUnitRunner.Silent.class)
public class RatingServiceImplTests {

    List<Rating> ratings = new ArrayList<>();

    UserDetail testUserDetail = new UserDetail();
    UserDetail creatingUserDetail = new UserDetail();
    private Beer testBeer = new Beer();
    private Rating testRating = new Rating();
    private final int testId = 1;


    @Mock
    RatingRepository repository;

    @InjectMocks
    RatingServiceImpl ratingService;


    @Before
    public void init() {


        Rating rating = new Rating();
        Rating rating2 = new Rating();
        rating.setId(testId);
        rating2.setId(2);
        rating.setReview("brbr");
        rating2.setReview("brbr");

        testUserDetail.setEmail("test@abv.bg");
        creatingUserDetail.setEmail("creating@abv.bg");

        rating.setRating(5);
        rating2.setRating(5);
        ratings.add(rating);
        ratings.add(rating2);

    }

    @Test
    public void deleteRatingShould_deleteRating_whenUserCreatedIt() {
        //Arrange

        ratings.get(0).setUserDetail(testUserDetail);
        //Act
        ratingService.deleteRating(ratings.get(0), testUserDetail.getEmail());
        //Assert
        Mockito.verify(repository, Mockito.times(1)).deleteRating(ratings.get(0));
    }

    @Test
    public void deleteRatingShould_throwError_whenUserNotCreated() {
        //Arrange

        ratings.get(0).setUserDetail(creatingUserDetail);
        //Act
        Assertions.assertThrows(InvalidOperationException.class,
                () -> ratingService.deleteRating(ratings.get(0), testUserDetail.getEmail()));
    }

    @Test
    public void getByIdShould_ReturnRating_WhenRatingExists() {
        //Arrange
        Rating expectedRating =ratings.get(0);

        Mockito.when(repository.getById(testId)).thenReturn(expectedRating);
        //Act
        Rating returnedRating  = ratingService.getById(testId);
        //Assert
        Assert.assertSame(expectedRating,returnedRating );
    }

    @Test
    public void getByIdShould_throwError_WhenRatingNotExists() {
        //Arrange

        Mockito.when(repository.getById(testId)).thenThrow(EntityNotFoundException.class);
        //Act
        //Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> ratingService.getById(testId));
    }

    @Test
    public void createRatingShould_ReturnRating_WhenUserNotRatedThisBeer() {
        //Arrange
        Rating expectedRating =ratings.get(0);

        Mockito.when(repository.createRating(ratings.get(0))).thenReturn(expectedRating);
        //Act
        Rating returnedRating  = ratingService.createRating(ratings.get(0), testUserDetail,testBeer);
        //Assert
        Assert.assertSame(expectedRating,returnedRating );
    }

    @Test
    public void createRatingShould_ThrowError_WhenUserAlreadyRate() {
        //Arrange
        ratings.get(0).setBeer(testBeer);
        ratings.get(0).setUserDetail(testUserDetail);
          testBeer.getBeerRatings().add(ratings.get(0));

        Mockito.when(repository.createRating(ratings.get(0))).thenThrow(InvalidOperationException.class);

        //Act ,Assert
        Assertions.assertThrows(InvalidOperationException.class,
                () -> ratingService.createRating(ratings.get(0), testUserDetail,testBeer));
    }

    @Test
    public void updateRatingShould_ReturnRating_WhenUserHasAuthority() {
        //Arrange
        Rating expectedRating =ratings.get(0);
        expectedRating.setUserDetail(testUserDetail);

        Mockito.when(repository.updateRating(ratings.get(0))).thenReturn(expectedRating);
        //Act
        Rating returnedRating  = ratingService.updateRating(testUserDetail.getEmail(),ratings.get(0));
        //Assert
        Assert.assertSame(expectedRating,returnedRating );

    }

    @Test
    public void updateRatingShould_throwError_WhenUserNot_Rated() {
        //Arrange
        Rating expectedRating =ratings.get(0);
        expectedRating.setUserDetail(creatingUserDetail);

        Mockito.when(repository.updateRating(expectedRating)).thenThrow(InvalidOperationException.class);
        //Act
        //Assert
        Assertions.assertThrows(InvalidOperationException.class,
                () -> ratingService.updateRating(testUserDetail.getEmail(),ratings.get(0)));
    }

    @Test
    public void getAllRatings_Should_ReturnAllRatings() {
        // Arrange
        Mockito.when(repository.getAll()).thenReturn(Arrays.asList(
                new Rating(5,"brbar"),
                new Rating(2,"brbassar")
        ));
        // Act
        List<Rating> result = ratingService.getAll();

        // Assert
        Assert.assertEquals(2, result.size());

    }


}
