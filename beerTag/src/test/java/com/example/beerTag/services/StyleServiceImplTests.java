package com.example.beerTag.services;

import com.example.beerTag.Factory;
import com.example.beerTag.entities.Beer;
import com.example.beerTag.entities.Style;
import com.example.beerTag.entities.UserDetail;
import com.example.beerTag.exceptions.DuplicateEntityException;
import com.example.beerTag.repositories.StyleRepositoryImpl;
import com.example.beerTag.repositories.StylesRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.doThrow;

@RunWith(MockitoJUnitRunner.class)
public class StyleServiceImplTests {

    private List<UserDetail> users;
    private UserDetail testUser = new UserDetail();
    private Beer testBeer = new Beer();
    private final int testId = 1;
    private Style style = new Style();

    @Mock
    StylesRepository stylesRepository;

    @InjectMocks
   StyleServiceImpl styleService;

    @Test
    public void deleteStyleShould_DeleteStyle_whenStyleExist() {
        //Arrange

        //Act
        styleService.deleteStyle(style);
        //Assert
        Mockito.verify(stylesRepository, Mockito.times(1))
                .deleteStyle(style);
    }

    @Test
    public void updateStyleShould_updateStyle_whenStyleExist() {


        //Arrange Act
        styleService.updateStyle(style);
        //Assert
        Mockito.verify(stylesRepository, Mockito.times(1))
                .updateStyle(style);
    }

    @Test
    public void createStyle_shouldCreateStyle_WhenStyleNotExists() {
        //Arrange Act
        styleService.createStyle(style);
        //Assert
        Mockito.verify(stylesRepository, Mockito.times(1))
                .createStyle(style);
    }

    @Test(expected= DuplicateEntityException.class )
    public void createStyleShould_ThrowError_WhenStyleAlreadyExists() {

        doThrow(new DuplicateEntityException())
                .when(stylesRepository)
                .createStyle(style);

        styleService.createStyle(style);

    }



    @Test
    public void getByIdShould_ReturnStyle_WhenStyleExists() {
        //Arrange
        Style expectedStyle = Factory.createStyle();

        Mockito.when(styleService.getById(testId)).thenReturn(expectedStyle);
        //Act
        Style style = styleService.getById(testId);
        //Assert
        Assert.assertSame(expectedStyle, style);
    }

    @Test
    public void getAllStyles_Should_ReturnAllStyles() {
        // Arrange
        Mockito.when(stylesRepository.getAll("")).thenReturn(Arrays.asList(
                new Style(),
                new Style()
        ));
        // Act
        List<Style> result =styleService.getAll("");

        // Assert
        Assert.assertEquals(2, result.size());
    }

}
