package com.example.beerTag.helper;

import com.example.beerTag.entities.Beer;
import com.example.beerTag.entities.UserDetail;

public class CheckHelper {

    public static boolean checkIfUserExist(UserDetail userDetail) {
        return userDetail ==null || !userDetail.getLogin().isEnabled();
    }

   public static boolean checkIfBeerExist(Beer beer) {
        return beer==null || beer.isDeleted();
    }

}
