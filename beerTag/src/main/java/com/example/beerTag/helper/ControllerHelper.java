package com.example.beerTag.helper;

import com.example.beerTag.entities.Beer;
import com.example.beerTag.entities.Brewery;
import com.example.beerTag.models.BreweryDto;
import com.example.beerTag.services.BeerServices;
import com.example.beerTag.services.BreweryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Component
public class ControllerHelper {

    private BeerServices beerServices;
    private BreweryService breweryService;

    @Autowired
    public ControllerHelper(BeerServices beerServices, BreweryService breweryService) {
        this.beerServices = beerServices;
        this.breweryService = breweryService;
    }

    public <T> Page<T> getItemsPaged(T itemType, Pageable pageable, String... parameters) {
        List items = new ArrayList<>();
        if (itemType instanceof Beer) {
            items = beerServices.getAll(parameters[0], parameters[1], parameters[2], parameters[3], parameters[4], parameters[5], parameters[6], parameters[7]);
        } else if (itemType instanceof Brewery) {
            items = breweryService.getAll(parameters[0], parameters[1]);
        }

        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List itemsSublist;
        if (items.size() < startItem) {
            itemsSublist = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, items.size());
            itemsSublist = items.subList(startItem, toIndex);
        }
        return new PageImpl<T>(itemsSublist, PageRequest.of(currentPage, pageSize), items.size());
    }

    public <T> void savePicture(T object, MultipartFile file) {
        String FILE_SAVE_DIR = "/media/uploads/";
        String DEFAULT_BEER_PIC = FILE_SAVE_DIR + "beer-example.jpg";
        String DEFAULT_BREWERY_PIC = FILE_SAVE_DIR + "brewery-example.jpg";

        String[] allowedFileTypes = {".png", ".gif", ".jpg"};
        String uploadedFileName = file.getOriginalFilename();

        assert uploadedFileName != null;
        if (!file.isEmpty() && Arrays.stream(allowedFileTypes).parallel().anyMatch(uploadedFileName.toLowerCase()::contains)) {
            try {
                String path = new File("src/main/media/uploads").getAbsolutePath();
                Path fileNameAdnPath = Paths.get(path, uploadedFileName);
                int existingFileCounter = 1;
                while (Files.exists(fileNameAdnPath)) {

                    if (existingFileCounter == 1) {
                        uploadedFileName = String.format("(%d)", existingFileCounter)
                                .concat(uploadedFileName);
                    }
                    uploadedFileName = uploadedFileName.replaceFirst("\\(+\\d+\\)", String.format("(%d)", existingFileCounter));
                    fileNameAdnPath = Paths.get(path, uploadedFileName);
                    existingFileCounter++;
                }
                Files.write(fileNameAdnPath, file.getBytes());

                if (object instanceof Beer) {
                    ((Beer) object).setPicture(FILE_SAVE_DIR + uploadedFileName);
                } else if (object instanceof Brewery) {
                    ((Brewery) object).setPicture(FILE_SAVE_DIR + uploadedFileName);
                } else if (object instanceof BreweryDto) {
                    ((BreweryDto)object).setPhoto(FILE_SAVE_DIR + uploadedFileName);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            if (object instanceof Beer) {
                ((Beer) object).setPicture(DEFAULT_BEER_PIC);
            } else if (object instanceof Brewery) {
                ((Brewery) object).setPicture(DEFAULT_BREWERY_PIC);
            } else if (object instanceof BreweryDto) {
                ((BreweryDto)object).setPhoto(DEFAULT_BREWERY_PIC);
            }
        }
    }
}
