package com.example.beerTag.repositories;

import com.example.beerTag.entities.Beer;
import com.example.beerTag.exceptions.DuplicateEntityException;
import com.example.beerTag.exceptions.EntityNotFoundException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.object.SqlQuery;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Repository
public class BeerRepositoryImpl implements BeerRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public BeerRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Beer> getAll(String name, String breweryName, String countryName, String styleName, String abv, String rating, String sortBy, String tagID) {
        try (Session session = sessionFactory.openSession()) {
            if (tagID != null && !tagID.equals("")) {
                SQLQuery beerByTag = session.createSQLQuery("Select * from  beers join beer_tags bt on beers.beer_id = bt.beer_id where tag_id = :tagID and is_deleted = false");
                beerByTag.addEntity(Beer.class);
                beerByTag.setParameter("tagID", Integer.valueOf(tagID));
                return beerByTag.list();
            } else {
                StringBuilder queryString = new StringBuilder("from Beer where name like :name" +
                        " and brewery.name like :breweryName" +
                        " and  brewery.country.name like :countryName" +
                        " and style.name like :styleName and isDeleted = false");
                if (abv != null && !abv.equals("")) {
                    queryString.append(" and abv = :abv");
                }
                if (rating != null && !rating.equals("")) {
                    queryString.append(" and averageRating = :rating");
                }

                switch (sortBy) {
                    case "name":
                        queryString.append(" order by name desc");
                        break;
                    case "country":
                        queryString.append(" order by brewery.country.name desc");
                        break;
                    case "brewery":
                        queryString.append(" order by brewery.name desc");
                        break;
                    case "style":
                        queryString.append(" order by style.name desc");
                        break;
                    default:
                        queryString.append(" order by id desc");
                }

                Query<Beer> query = session.createQuery(queryString.toString(), Beer.class);
                query.setParameter("name", "%" + name + "%");
                query.setParameter("breweryName", "%" + breweryName + "%");
                query.setParameter("countryName", "%" + countryName + "%");
                query.setParameter("styleName", "%" + styleName + "%");

                if (abv != null && !abv.equals("")) {
                    query.setParameter("abv", Double.valueOf(abv));
                }
                if (rating != null && !rating.equals("")) {
                    query.setParameter("rating", Integer.valueOf(rating));
                }

                return query.list();
            }
        }
    }

    @Override
    public Beer getBeerById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer where id = :id and isDeleted = false", Beer.class);
            return query.setParameter("id", id).getSingleResult();
        } catch (NoResultException e) {
            throw new EntityNotFoundException("Beer", id);
        }
    }

    @Override
    public Beer createBeer(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer where name = :name and isDeleted = false", Beer.class);
            Beer existingBeer = query.setParameter("name", beer.getName()).stream().findFirst().orElse(null);
            if (existingBeer != null) {
                throw new DuplicateEntityException("Beer", "Name", beer.getName());
            } else {
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date date = new Date();
                beer.setDateCreated(dateFormat.format(date));
                session.save(beer);
                return beer;
            }
        }
    }

    @Override
    public void updateBeer(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(beer);
            session.getTransaction().commit();
        }
    }

    @Override
    public void deleteBeer(int id) {
        try (Session session = sessionFactory.openSession()) {
            Beer beerToDelete = session.get(Beer.class, id);
            if (beerToDelete == null) {
                throw new EntityNotFoundException("Beer", id);
            } else {
                beerToDelete.setDeleted(true);
                session.beginTransaction();
                session.update(beerToDelete);
                session.getTransaction().commit();
            }
        }
    }
}
