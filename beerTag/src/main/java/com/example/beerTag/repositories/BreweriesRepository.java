package com.example.beerTag.repositories;

import com.example.beerTag.entities.Brewery;
import com.example.beerTag.models.BreweryDto;

import java.util.List;

public interface BreweriesRepository {

    List<Brewery> getAll(String name, String countryName);

    Brewery getById(int id);

    Brewery createBrewery(Brewery brewery);

    Brewery updateBrewery(int id, Brewery brewery);

    void deleteBrewery(int id);

    boolean checkIfExists(int id);

    Brewery checkIfExists(String name);
}
