package com.example.beerTag.repositories;

import com.example.beerTag.exceptions.DuplicateEntityException;
import com.example.beerTag.exceptions.EntityNotFoundException;
import com.example.beerTag.entities.Brewery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Repository
public class BreweriesRepositoryImpl implements BreweriesRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public BreweriesRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Brewery> getAll(String name, String countryName) {
        try (Session session = sessionFactory.openSession()) {
            Query<Brewery> query = session.createQuery("from Brewery where name like :name " +
                    "and country.name like :countryName " +
                    "and isDeleted = false order by id desc ", Brewery.class);
            query.setParameter("name", "%" + name + "%");
            query.setParameter("countryName", "%" + countryName + "%");
            return query.list();
        }
    }

    @Override
    public Brewery getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Brewery> query = session.createQuery("from Brewery where id = :id and isDeleted = false", Brewery.class);
            query.setParameter("id", id);
            Brewery brewery = query.stream().findFirst().orElse(null);
            if (brewery == null) {
                throw new EntityNotFoundException("Brewery", id);
            }
            return brewery;
        }
    }

    @Override
    public Brewery createBrewery(Brewery brewery) {
        try (Session session = sessionFactory.openSession()) {
            Brewery existingBrewery = checkIfExists(brewery.getName());
            if (existingBrewery != null) {
                if (existingBrewery.isDeleted()) {
                    existingBrewery.setDeleted(false);
                    session.beginTransaction();
                    session.update(existingBrewery);
                    session.getTransaction().commit();
                    return existingBrewery;
                } else {
                    throw new DuplicateEntityException("Brewery", "Name", brewery.getName());
                }
            } else {
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date date = new Date();
                brewery.setDateCreated(dateFormat.format(date));
                session.save(brewery);
                return brewery;
            }
        }
    }

    @Override
    public Brewery updateBrewery(int id, Brewery brewery) {
        try (Session session = sessionFactory.openSession()) {
            if (!checkIfExists(id)) {
                throw new EntityNotFoundException("Brewery", id);
            } else {
                session.beginTransaction();
                session.update(brewery);
                session.getTransaction().commit();
                return brewery;
            }
        }
    }

    @Override
    public void deleteBrewery(int id) {
        try (Session session = sessionFactory.openSession()) {
            Brewery brewery = session.get(Brewery.class, id);
            if (!checkIfExists(id)) {
                throw new EntityNotFoundException("Brewery", id);
            } else {
                brewery.setDeleted(true);
                session.beginTransaction();
                session.update(brewery);
                session.getTransaction().commit();
            }
        }
    }

    @Override
    public boolean checkIfExists(int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Brewery.class, id) != null;
        }
    }

    @Override
    public Brewery checkIfExists(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Brewery> query = session.createQuery("from Brewery where name like :name", Brewery.class);
            return query.setParameter("name", name).stream().findFirst().orElse(null);
        }
    }
}
