package com.example.beerTag.repositories;

import com.example.beerTag.exceptions.DuplicateEntityException;
import com.example.beerTag.exceptions.EntityNotFoundException;
import com.example.beerTag.entities.Tag;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Repository
public class TagRepositoryImpl implements TagRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public TagRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Tag> getAll(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createQuery("from Tag where name like :name and isDeleted = false", Tag.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }

    @Override
    public Tag getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createQuery("from Tag where id = :id and isDeleted = false", Tag.class);
            query.setParameter("id", id);
            Tag tag = query.stream().findFirst().orElse(null);
            if (tag == null) {
                throw new EntityNotFoundException("Tag", id);
            } else {
                return tag;
            }
        }
    }

    @Override
    public Tag createTag(Tag tag) {
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createQuery("from Tag where name = :name and isDeleted = false", Tag.class);
            Tag existingTag = query.setParameter("name", tag.getName()).stream().findFirst().orElse(null);
            if (existingTag != null) {
                throw new DuplicateEntityException("Tag", "Name", tag.getName());
            } else {
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date date = new Date();
                tag.setDateCreated(dateFormat.format(date));
                session.save(tag);
                return tag;
            }
        }
    }


    @Override
    public Tag updateTag(int id, Tag tag) {
        try (Session session = sessionFactory.openSession()) {
            Tag existingTag = session.get(Tag.class, id);
            if (existingTag == null) {
                throw new EntityNotFoundException("Tag", id);
            } else {
                session.evict(existingTag);
                session.beginTransaction();
                session.update(tag);
                session.getTransaction().commit();
                return tag;
            }
        }
    }

    @Override
    public void deleteTag(int id) {
        try (Session session = sessionFactory.openSession()) {
            Tag tag = session.get(Tag.class, id);
            if (tag == null) {
                throw new EntityNotFoundException("Tag", id);
            } else {
                tag.setDeleted(true);
                session.beginTransaction();
                session.update(tag);
                session.getTransaction().commit();
            }
        }
    }
}
