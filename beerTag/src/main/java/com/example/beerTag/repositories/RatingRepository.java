package com.example.beerTag.repositories;

import com.example.beerTag.entities.Rating;

import java.util.List;

public interface RatingRepository {

    List<Rating> getAll();
   // List<Rating> getBeerAllRatings(int beerId);

    Rating createRating(Rating rating);

    Rating updateRating(Rating rating);

    void deleteRating(Rating rating);

    Rating getById(int id);


}
