package com.example.beerTag.repositories;

import com.example.beerTag.entities.Style;

import java.util.List;

public interface StylesRepository {

    List<Style> getAll(String name);

    Style getById(int id);

   // List<Style> getByName(String name);

    void createStyle(Style style);

    void updateStyle(Style style);

    void deleteStyle(Style style);


}
