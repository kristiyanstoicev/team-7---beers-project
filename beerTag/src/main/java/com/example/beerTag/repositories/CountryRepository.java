package com.example.beerTag.repositories;

import com.example.beerTag.entities.Country;

import java.util.List;

public interface CountryRepository {

    List<Country> getAll();

    Country getById(int id);

    List<Country> filteredByName(String name);

    Country createCountry(Country country);

    Country updateCountry(int id,Country country);

    void deleteCountry(int id);

    boolean checkIfExists(int id);
}
