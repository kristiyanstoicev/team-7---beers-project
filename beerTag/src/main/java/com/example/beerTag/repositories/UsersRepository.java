package com.example.beerTag.repositories;

import com.example.beerTag.entities.Beer;
import com.example.beerTag.entities.UserDetail;

import java.util.List;

public interface UsersRepository  {


    List<UserDetail> getAll(String firstName, String lastName, String sortBy);

    void addBeerToDrankList(UserDetail userDetail, int beerId);

     void addBeerToWishList(UserDetail userDetail, int beerId);

    void deleteBeerFromList(UserDetail userDetail, int beerId);

   List <Beer> getTopThreeRated(UserDetail userDetail);

     List<Beer> getList(UserDetail userDetail, String typeOfList);

    UserDetail getByEmail(String email);

    UserDetail getById(int id);

    void createUser(UserDetail userDetail);


    UserDetail updateUser(UserDetail userDetail);

    void deleteUser(int userId);

    //   void addBeerToWishList(Beer beer,User user);
    //   void addBeerToDrankList(Beer beer, User user);
    //   List<Beers> getWishList();
    //   List<Beers> getDrankList();

}
