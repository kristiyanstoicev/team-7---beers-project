package com.example.beerTag.repositories;

import com.example.beerTag.entities.Login;

public interface LoginRepository {

   void deleteLogin (String username);
   Login getByUsername(String username);




}
