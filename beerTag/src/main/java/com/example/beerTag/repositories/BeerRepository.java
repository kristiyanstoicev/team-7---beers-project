package com.example.beerTag.repositories;

import com.example.beerTag.entities.Beer;

import java.util.List;

public interface BeerRepository {
    List<Beer> getAll(String name, String breweryName, String countryName, String styleName, String abv, String rating, String sortBy, String tagID);

    Beer getBeerById(int id);

    Beer createBeer(Beer beer);

    void updateBeer(Beer beer);

    void deleteBeer(int id);
}
