package com.example.beerTag.repositories;

import com.example.beerTag.entities.Style;
import com.example.beerTag.exceptions.DuplicateEntityException;
import com.example.beerTag.exceptions.EntityNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Repository;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class StyleRepositoryImpl implements StylesRepository {

    private SessionFactory sessionFactory;


    @Autowired
    public StyleRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Style> getAll(String name) {
        try (Session session = sessionFactory.openSession()) {

            Query<Style> query = session.createQuery("from Style where name LIKE :name" +
                    " and isDeleted = false", Style.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }

    @Override
    public Style getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Style style = session.get(Style.class, id);
            if (style == null || style.isDeleted()) {
                throw new EntityNotFoundException("Style", id);
            }
            return style;
        }
    }
//
//    @Override
//    public List<Style> getByName(String name) {
//        try (Session session = sessionFactory.openSession()) {
//            Query<Style> query = session
//                    .createQuery("from Style where name LIKE :name"+
//                   " and isDeleted = false");
//            query.setParameter("name", "%" + name + "%");
//           return query.list();
//        }
//    }

    @Override
    public void createStyle(Style style) {
        try (Session session = sessionFactory.openSession()) {
            Query<Style> query = session
                    .createQuery("from Style where name LIKE :name" +
                            " AND isDeleted=false", Style.class);
            query.setParameter("name", style.getName());
            if (!query.list().isEmpty()) {
                throw new DuplicateEntityException("Style", "name", style.getName());
            }
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            style.setDate(dateFormat.format(date));
            session.save(style);
        }
    }

    @Override
    public void updateStyle(Style style) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(style);
            session.getTransaction().commit();
        }
    }

    @Override
    public void deleteStyle(Style style) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(style);
            session.getTransaction().commit();
        }
    }


}
