package com.example.beerTag.repositories;

import com.example.beerTag.entities.Beer;
import com.example.beerTag.entities.UserDetail;
import com.example.beerTag.entities.UserBeersStatus;
import com.example.beerTag.exceptions.DuplicateEntityException;
import com.example.beerTag.exceptions.EntityNotFoundException;
import com.example.beerTag.helper.CheckHelper;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class UsersRepositoryImpl implements UsersRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public UsersRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<UserDetail> getAll(String firstName, String lastName, String sortBy) {

        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder("from UserDetail where firstName like :firstName" +
                    " and lastName like : lastName" +
                    " and login.enabled=true");

            switch (sortBy) {
                case "firstName":
                    queryString.append(" order by firstName asc");
                    break;
                case "lastName":
                    queryString.append(" order by LastName asc");
                    break;
                default:
                    queryString.append(" order by id desc");}


            Query<UserDetail> query = session.createQuery(String.valueOf(queryString),
                    UserDetail.class);
            query.setParameter("firstName", "%" + firstName + "%");
            query.setParameter("lastName", "%" + lastName+ "%");

            return query.list();

        }
    }

    @Override
    public UserDetail getByEmail(String email) {

        try (Session session = sessionFactory.openSession()) {
            Query<UserDetail> query = session
                    .createQuery("from UserDetail " +
                            " where email = :email", UserDetail.class);
            query.setParameter("email", email);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("User", "email", email);
            }
            return query.list().get(0);
        }
    }

    @Override
    public UserDetail getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            UserDetail userDetail = session.get(UserDetail.class, id);
            if (CheckHelper.checkIfUserExist(userDetail)) {
                throw new EntityNotFoundException("User", id);
            }
            return userDetail;
        }
    }


    @Override
    public void createUser(UserDetail userDetail) {
        try (Session session = sessionFactory.openSession()) {

            session.save(userDetail);
        }
    }


    @Override
    public UserDetail updateUser(UserDetail userDetail) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(userDetail);
            session.getTransaction().commit();
        }
        return userDetail;
    }

    @Override
    public void deleteUser(int userId) {
        try (Session session = sessionFactory.openSession()) {

            UserDetail user = session.get(UserDetail.class, userId);

            if(user==null||!user.getLogin().isEnabled()){
                throw new EntityNotFoundException("User",userId);
            }
            user.getLogin().setEnabled(false);

            session.beginTransaction();
            session.update(user.getLogin());
            session.update(user);
            session.getTransaction().commit();
        }
    }

    public void addBeerToDrankList(UserDetail userDetail, int beerId) {
        try (Session session = sessionFactory.openSession()) {

            Beer beer = session.get(Beer.class, beerId);

            if (CheckHelper.checkIfBeerExist(beer)) {
                throw new EntityNotFoundException("Beer", beerId);
            }
            //check if it is already in drank list
            if (userDetail.getDrankList().contains(beer)) {
                throw new DuplicateEntityException("Beer", "name", beer.getName());
            }
            //check if in wish list and set to be in drank list, without creating new one
            if (userDetail.getWishList().contains(beer)) {
                Query<UserBeersStatus> query = session
                        .createQuery("from UserBeersStatus where beerId = :beerId" +
                                " and userId= :id", UserBeersStatus.class)
                        .setParameter("beerId", beerId)
                        .setParameter("id", userDetail.getId());
                UserBeersStatus oldUserBeerStatus = query.list().get(0);
                oldUserBeerStatus.setStatusId(1); // from wish to drank
                session.beginTransaction();
                session.update(oldUserBeerStatus);
                session.getTransaction().commit();

            } else {
                UserBeersStatus drankList = new UserBeersStatus(userDetail.getId(), beerId, 1); // or just new one

                session.save(drankList);
            }

        }
    }


    public void addBeerToWishList(UserDetail userDetail, int beerId) {
        try (Session session = sessionFactory.openSession()) {
            Beer beer = session.get(Beer.class, beerId);
            if (CheckHelper.checkIfBeerExist(beer)) {
                throw new EntityNotFoundException("Beer", beerId);
            }
            //check if it is already in drank list
            if (userDetail.getWishList().contains(beer)) {
                throw new DuplicateEntityException("Beer", "name", beer.getName());
            }
            //check if in wish list and set to be in drank list, without creating new one
            if (userDetail.getDrankList().contains(beer)) {
                Query<UserBeersStatus> query = session
                        .createQuery("from UserBeersStatus where beerId = :beerId" +
                                " and userId= :id", UserBeersStatus.class)
                        .setParameter("beerId", beerId)
                        .setParameter("id", userDetail.getId());
                UserBeersStatus oldUserBeerStatus = query.list().get(0);

                oldUserBeerStatus.setStatusId(2); // from drank to wish
                session.beginTransaction();
                session.update(oldUserBeerStatus);
                session.getTransaction().commit();
            } else {
                UserBeersStatus wishList = new UserBeersStatus(userDetail.getId(), beerId, 2); // or just new one
                session.save(wishList);
            }

        }
    }

    public void deleteBeerFromList(UserDetail userDetail, int beerId) {
        try (Session session = sessionFactory.openSession()) {
            Beer beer = session.get(Beer.class, beerId);
            if (CheckHelper.checkIfBeerExist(beer)) {
                throw new EntityNotFoundException("Beer", beerId);
            }

            if (!userDetail.getDrankList().contains(beer) &&
                    !userDetail.getWishList().contains(beer)) {
                throw new EntityNotFoundException("Beer", beerId);
            } else {
                Query<UserBeersStatus> query = session
                        .createQuery("from UserBeersStatus where beerId = :beerId" +
                                " and userId= :id", UserBeersStatus.class)
                        .setParameter("beerId", beerId)
                        .setParameter("id", userDetail.getId());
                UserBeersStatus oldUserBeerStatus = query.list().get(0);

                session.beginTransaction();
                session.delete(oldUserBeerStatus);
                session.getTransaction().commit();
            }

        }
    }

    public List<Beer> getList(UserDetail userDetail, String typeOfList) {

        if (typeOfList.equals("drank")) {
            return userDetail.getDrankList().stream()
                    .filter(beer -> !beer.isDeleted())
                    .collect(Collectors.toList());
            // if it is wish list
        } else {
            return userDetail.getWishList().stream()
                    .filter(beer -> !beer.isDeleted())
                    .collect(Collectors.toList());
        }
    }

    public List<Beer> getTopThreeRated(UserDetail userDetail) {

       return userDetail.getDrankList()
                .stream()
                .sorted(Comparator.comparing(Beer::getAverageRating).reversed())
                .limit(3)
                .collect(Collectors.toList());
    }
}
