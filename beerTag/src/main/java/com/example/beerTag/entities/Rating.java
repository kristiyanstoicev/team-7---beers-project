package com.example.beerTag.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "ratings")
@Getter
@Setter
@NoArgsConstructor
public class Rating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_rating")
    private int id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserDetail userDetail;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "beer_id")
    private Beer beer;

    @Column(name = "rating")
    private int rating;

    @Column(name = "review")
    private String review;

    @Column(name = "date_created")
    private String dateCreated;

    // Necessary only for unit tests
    public Rating(int rating, String review) {
        this.rating = rating;
        this.review = review;
    }
}




