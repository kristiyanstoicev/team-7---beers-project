package com.example.beerTag.entities.common;

public enum RatingEnum {
    ZERO,
    ONE,
    TWO,
    THREE,
    FOUR,
    FIVE
}
