package com.example.beerTag.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "beers")
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Beer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "beer_id")
    @EqualsAndHashCode.Include
    private int id;

    @Column(name = "beer_name")
    private String name;

    @Column(name = "beer_description")
    private String description;

    @OneToOne
    @JoinColumn(name = "brewery_id")
    private Brewery brewery;

    @OneToOne
    @JoinColumn(name = "style_id")
    private Style style;

    @Column(name = "abv")
    private double abv;

    @Column(name = "date_added")
    private String dateCreated;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "beer_tags",
            joinColumns = @JoinColumn(name = "beer_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id"))
    private Set<Tag> tags = new HashSet<>();

    @Column(name = "beer_picture")
    private String picture;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserDetail userDetail;

    @Column(name = "average_rating")
    private int averageRating = 0;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    @LazyCollection(LazyCollectionOption.FALSE)
    @JsonIgnore
    @OneToMany(mappedBy = "beer")
    private List <Rating> beerRatings=new ArrayList<>();

    public Beer(String name, String description, Brewery brewery, Style style, double abv, String picture) {
        this.name = name;
        this.description = description;
        this.brewery = brewery;
        this.style = style;
        this.abv = abv;
        this.picture = picture;
    }
}
