package com.example.beerTag.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "breweries")
public class Brewery {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "brewery_id")
    private int id;

    @Column(name = "brewery_name")
    private String name;

    @Column(name = "date_created")
    private String dateCreated;

    @ManyToOne
    @JoinColumn(name = "country_id")
    private Country country;

    @Column(name = "brewery_picture")
    private String picture;

    @Column(name = "is_deleted")
    boolean isDeleted;
}
