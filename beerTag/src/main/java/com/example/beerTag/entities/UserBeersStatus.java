package com.example.beerTag.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users_beers_status")
public class UserBeersStatus {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="users_beers_status_id")
    int id;
    @Column(name = "user_id")
    int userId;
    @Column (name = "beer_id")
    int beerId;
    @Column (name = "status_id")
    int statusId;

    public UserBeersStatus(int userId, int beerId, int statusId) {
        this.userId = userId;
        this.beerId = beerId;
        this.statusId = statusId;
    }
}
