package com.example.beerTag.services;

import com.example.beerTag.entities.Brewery;

import java.util.List;

public interface BreweryService {

    List<Brewery> getAll(String name, String countryName);

    Brewery getById(int id);

    Brewery createBrewery(Brewery brewery);

    Brewery updateBrewery(int id, Brewery brewery);

    void deleteBrewery(int id);
}
