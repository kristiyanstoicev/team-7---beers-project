package com.example.beerTag.services;

import com.example.beerTag.entities.Beer;
import com.example.beerTag.entities.Tag;

import java.util.List;

public interface BeerServices {
    List<Beer> getAll(String name, String breweryName, String countryName, String styleName, String abv, String rating, String sortBy, String tagID);

    Beer getBeerById(int id);

    Beer createBeer(Beer beer);

    void updateBeer(Beer beer);

    void deleteBeer(int id);
}
