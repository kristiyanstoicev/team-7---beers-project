package com.example.beerTag.services;

import com.example.beerTag.entities.Beer;
import com.example.beerTag.entities.UserDetail;
import com.example.beerTag.exceptions.DuplicateEntityException;
import com.example.beerTag.exceptions.EntityNotFoundException;
import com.example.beerTag.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserServiceImpl implements UserService {

    private UsersRepository repository;

    @Autowired
    public UserServiceImpl(UsersRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<UserDetail> getAll(String firstName, String lastName, String sortBy) {
        return repository.getAll( firstName,  lastName,  sortBy);
    }

    @Override
    public UserDetail getByEmail(String email) {
        return repository.getByEmail(email);
    }

    @Override
    public List<Beer> getTopThreeRated(UserDetail userDetail) {
        return repository.getTopThreeRated(userDetail);
    }

    @Override
    public UserDetail getById(int id) {
        try {
            return repository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("User", id);
        }
    }


    @Override
    public void createUser(UserDetail userDetail) {
           repository.createUser(userDetail);

    }


    @Override
    public UserDetail updateUser(UserDetail userDetail) {
      return   repository.updateUser(userDetail);
    }

    @Override
    public void deleteUser(int id) {

        try {
            repository.deleteUser(id);
        } catch (EntityNotFoundException e) {
         throw new EntityNotFoundException("User",id);
        }
    }

    @Override
    public void addBeerToDrankList(UserDetail userDetail, int beerId) {

        try {
            repository.addBeerToDrankList(userDetail, beerId);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Beer",beerId);
        } catch (DuplicateEntityException e){
        throw new DuplicateEntityException("Beer");
    }
    }

    @Override
    public void addBeerToWishList(UserDetail userDetail, int beerId) {

        try {
            repository.addBeerToWishList(userDetail, beerId);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Beer",beerId);
        }catch (DuplicateEntityException e){
            throw new DuplicateEntityException("Beer");
        }
    }

    @Override
    public void deleteBeerFromList(UserDetail userDetail, int beerId) {
        try {
            repository.deleteBeerFromList(userDetail,beerId);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException("Beer",beerId);
        }
    }

    @Override
    public List<Beer> getList(UserDetail userDetail, String typeOfList) {
       return repository.getList(userDetail,typeOfList);
    }
}
