package com.example.beerTag.services;

import com.example.beerTag.entities.Beer;
import com.example.beerTag.entities.Style;
import com.example.beerTag.models.StyleDto;

import java.util.List;

public interface StyleService {

    List<Style> getAll(String name);

    Style getById(int id);

    void createStyle(Style style);

    void updateStyle(Style style);

    void deleteStyle(Style style);

}
