package com.example.beerTag.services;

import com.example.beerTag.entities.Login;
import com.example.beerTag.repositories.LoginRepository;
import org.springframework.stereotype.Service;

@Service
public class LoginServicesImpl implements LoginServices {

    LoginRepository loginRepository;

    public LoginServicesImpl(LoginRepository loginRepository) {
        this.loginRepository = loginRepository;
    }


    @Override
    public Login getByUsername(String email) {
        return loginRepository.getByUsername(email);
    }
}
