package com.example.beerTag.models;

import com.example.beerTag.entities.common.RatingEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor// (access = AccessLevel.PRIVATE)
public class RatingDto {

    private int rating;

    private String review;

}
