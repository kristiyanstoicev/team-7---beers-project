package com.example.beerTag.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BreweryDto {

    @Size(min = 5, max = 20, message = "Brewery name must be between {min} and {max} characters long.")
    private String name;

    private int countryId;

    private String photo;
}
