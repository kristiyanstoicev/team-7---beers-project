package com.example.beerTag.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;


@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor// (access = AccessLevel.PRIVATE)
public class UserDto {

    @Size(min = 2,
            max =30,
            message = "Name size should not be less than {min} and more than {max} characters long")
    private String firstName;

    @Size(min = 2,
            max=30,
            message = "Name size should not be less than {min} and more than {max} characters long")
    private String lastName;

    @Email(message = "Email should be valid")
    private String email;

//    @Size(min = 6,
//            max = 30,
//            message = "Password should not be less than {min} and more than {max} characters long")
    private String password;
//    @Size(min = 6,
//            max = 30,
//            message = "Password should not be less than {min} and more than {max} characters long")
    private String passwordConfirmation;

    private int countryId;
}
