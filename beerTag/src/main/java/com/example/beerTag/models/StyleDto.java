package com.example.beerTag.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor// (access = AccessLevel.PRIVATE)

public class StyleDto {
    private String name;
    private String description;
}
