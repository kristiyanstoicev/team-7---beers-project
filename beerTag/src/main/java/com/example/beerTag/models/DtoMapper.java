package com.example.beerTag.models;

import com.example.beerTag.entities.*;
import com.example.beerTag.repositories.*;
import com.example.beerTag.services.LoginServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class DtoMapper {

    private StylesRepository stylesRepository;
    private BreweriesRepository breweriesRepository;
    private CountryRepository countryRepository;
    private TagRepository tagRepository;
    private RatingRepository ratingRepository;
    private LoginServices loginServices;

    @Autowired
    public DtoMapper(StylesRepository stylesRepository, BreweriesRepository breweriesRepository, CountryRepository countryRepository, TagRepository tagRepository,
                     RatingRepository ratingRepository, LoginServices loginServices) {
        this.stylesRepository = stylesRepository;
        this.breweriesRepository = breweriesRepository;
        this.countryRepository = countryRepository;
        this.tagRepository = tagRepository;
        this.ratingRepository=ratingRepository;
        this.loginServices = loginServices;
    }

    public Brewery fromDto(BreweryDto breweryDto) {
        Brewery newBrewery = new Brewery();
        newBrewery.setName(breweryDto.getName());
        newBrewery.setCountry(countryRepository.getById(breweryDto.getCountryId()));
        newBrewery.setPicture(breweryDto.getPhoto());
        return newBrewery;
    }

    public Brewery updateBreweryFromDto(Brewery brewery, BreweryDto breweryDto) {
        brewery.setName(breweryDto.getName());
        brewery.setCountry(countryRepository.getById(breweryDto.getCountryId()));
        return brewery;
    }

    public Beer fromDto(BeerDto beerDto) {
        return new Beer(beerDto.getName(), beerDto.getDescription(),
                breweriesRepository.getById(beerDto.getBreweryId()),
                stylesRepository.getById(beerDto.getStyleId()),
                beerDto.getAbv(), beerDto.getPictureURL());
    }

    public Beer updateBeerFromDto(Beer beer, BeerDto beerDto) {
        beer.setName(beerDto.getName());
        beer.setDescription(beerDto.getDescription());
        beer.setStyle(stylesRepository.getById(beerDto.getStyleId()));
        beer.setBrewery(breweriesRepository.getById(beerDto.getBreweryId()));
        beer.setAbv(beerDto.getAbv());
//        beer.setPicture(beerDto.getPictureURL());
        return beer;
    }

    public UserDetail fromDto(UserDto userDto) {

        UserDetail newUserDetail = new UserDetail(userDto.getFirstName(), userDto.getLastName(),
                userDto.getEmail());
        newUserDetail.setCountry(countryRepository.getById(userDto.getCountryId()));
        newUserDetail.setLogin(loginServices.getByUsername(userDto.getEmail()));
        newUserDetail.setPhoto("/media/uploads/default-photo.jpg");
        return newUserDetail;
    }

    //skip setting email, because is immutable
    public UserDetail updateUserFromDto(UserDto userDto, UserDetail userDetailToUpdate) {

        userDetailToUpdate.setFirstName(userDto.getFirstName());
        userDetailToUpdate.setLastName(userDto.getLastName());
        Country country = countryRepository.getById(userDto.getCountryId());
        userDetailToUpdate.setCountry(country);
        return userDetailToUpdate;
    }

    public Rating createRatingFromDto(RatingDto ratingDto){
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        Rating rating = new Rating();
        rating.setReview(ratingDto.getReview());
        rating.setRating(ratingDto.getRating());
        rating.setDateCreated(dateFormat.format(date));

        return rating;
    }

    public Rating updateRatingFromDto(RatingDto ratingDto, int id){
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        Rating rating = ratingRepository.getById(id);
        rating.setReview(ratingDto.getReview());
        rating.setRating(ratingDto.getRating());
        rating.setDateCreated(dateFormat.format(date));

        return rating;

    }
}
