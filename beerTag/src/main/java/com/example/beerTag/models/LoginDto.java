package com.example.beerTag.models;

import com.example.beerTag.entities.Authority;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginDto {

    private int id;

    private String username;

    private String password;

    private String passwordConfirmation;

    private Authority role;
}
