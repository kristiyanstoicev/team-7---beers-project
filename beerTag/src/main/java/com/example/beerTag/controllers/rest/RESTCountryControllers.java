package com.example.beerTag.controllers.rest;

import com.example.beerTag.exceptions.DuplicateEntityException;
import com.example.beerTag.exceptions.EntityNotFoundException;
import com.example.beerTag.entities.Country;
import com.example.beerTag.services.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/countries")
public class RESTCountryControllers {

    private CountryService services;

    @Autowired
    public RESTCountryControllers(CountryService services) {
        this.services = services;
    }

    @GetMapping
    List<Country> getAll(@RequestParam(value = "name", required = false) String name) {
        if (name != null && !name.isEmpty()) {
            return services.getCountriesByName(name);
        }
        return services.getAll();
    }

    @GetMapping("/{id}")
    public Country getById(@PathVariable int id) {
        try {
            return services.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Country createCountry(@RequestBody Country country) {
        try {
            return services.createCountry(country);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Country updateCountry(@PathVariable int id, @RequestBody Country country) {
        try {
            Country newCountry = services.getById(id);
            newCountry.setName(country.getName());
            return services.updateCountry(id, newCountry);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void deleteBrewery(@PathVariable int id) {
        try {
            services.deleteCountry(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
