package com.example.beerTag.controllers.rest;

import com.example.beerTag.entities.UserDetail;
import com.example.beerTag.exceptions.DuplicateEntityException;
import com.example.beerTag.exceptions.EntityNotFoundException;
import com.example.beerTag.entities.Tag;
import com.example.beerTag.services.TagService;
import com.example.beerTag.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/tags")
public class RESTTagControllers {

    private TagService tagService;
    private UserService userService;

    @Autowired
    public RESTTagControllers(TagService services, UserService userService) {
        this.tagService = services;
        this.userService = userService;
    }

    @GetMapping
    public List<Tag> getAll(@RequestParam(value = "name", defaultValue = "") String name) {
            return tagService.getAll(name);
    }

    @GetMapping("/{id}")
    public Tag getById(@PathVariable int id) {
        try {
            return tagService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Tag createTag(@RequestBody @Valid Tag tag,
                         @RequestHeader(name = "Authorization") String userEmail) {
        try {
            UserDetail userDetail = userService.getByEmail(userEmail);
            tagService.createTag(tag);
            return tag;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Tag updateTag(@PathVariable int id, @RequestBody @Valid Tag tag,
                         @RequestHeader(name = "Authorization") String userEmail) {
        try {
            UserDetail userDetail = userService.getByEmail(userEmail);
            Tag newTag = tagService.getById(id);
            newTag.setName(tag.getName());
            tagService.updateTag(id, newTag);
            return newTag;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void deleteTag(@PathVariable int id,
                          @RequestHeader(name = "Authorization") String userEmail) {
        try {
            UserDetail userDetail = userService.getByEmail(userEmail);
            tagService.deleteTag(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}


