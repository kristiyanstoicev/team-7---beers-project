package com.example.beerTag.controllers;

import com.example.beerTag.entities.*;
import com.example.beerTag.exceptions.DuplicateEntityException;
import com.example.beerTag.exceptions.EntityNotFoundException;
import com.example.beerTag.helper.ControllerHelper;
import com.example.beerTag.models.BeerDto;
import com.example.beerTag.models.DtoMapper;
import com.example.beerTag.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
public class BeersController {
    private BeerServices beerServices;
    private StyleService styleService;
    private BreweryService breweryService;
    private UserService userService;
    private TagService tagService;
    private RatingService ratingService;
    private CountryService countryService;
    private DtoMapper dtoMapper;
    private ControllerHelper controllerHelper;

    @Autowired
    public BeersController(BeerServices beerServices, StyleService styleService, BreweryService breweryService,
                           UserService userService, TagService tagService, RatingService ratingService,
                           CountryService countryService, DtoMapper dtoMapper, ControllerHelper controllerHelper) {
        this.beerServices = beerServices;
        this.styleService = styleService;
        this.breweryService = breweryService;
        this.userService = userService;
        this.tagService = tagService;
        this.ratingService = ratingService;
        this.countryService = countryService;
        this.dtoMapper = dtoMapper;
        this.controllerHelper = controllerHelper;
    }

    @ModelAttribute("styles")
    public List<Style> populateStyles() {
        return styleService.getAll("");
    }

    @ModelAttribute("breweries")
    public List<Brewery> populateBreweries() {
        return breweryService.getAll("", "");
    }

    @ModelAttribute("tags")
    public List<Tag> populateTags() {
        return tagService.getAll("");
    }

    @ModelAttribute("countries")
    public List<Country> populateCountries() {
        return countryService.getAll();
    }


    @GetMapping("/beers")
    public String showAllBeers(@RequestParam(name = "name", defaultValue = "") String name,
                               @RequestParam(name = "brewery", defaultValue = "") String brewery,
                               @RequestParam(name = "style", defaultValue = "") String style,
                               @RequestParam(name = "country", defaultValue = "") String country,
                               @RequestParam(name = "abv", defaultValue = "") String abv,
                               @RequestParam(name = "rating", defaultValue = "") String rating,
                               @RequestParam(name = "sortBy", defaultValue = "") String sortBy,
                               @RequestParam(name = "tagID", defaultValue = "") String tagID,
                               @RequestParam(name = "page", defaultValue = "1") int currentPage,
                               @RequestParam(name = "size", defaultValue = "6") int pageSize,
                               Model model, Principal principal, @RequestParam(value = "files", required = false) MultipartFile file) {

        if (principal != null) {
            model.addAttribute("user", userService.getByEmail(principal.getName()));
        }
        Page<Beer> beerPage = controllerHelper.getItemsPaged(new Beer(),
                PageRequest.of(currentPage - 1, pageSize),
                name, brewery, country, style, abv, rating, sortBy, tagID);

        model.addAttribute("beerPage", beerPage);
        model.addAttribute("beer", new Beer());
        model.addAttribute("style", new Style());
        model.addAttribute("nameFilter", name);
        model.addAttribute("breweryFilter", brewery);
        model.addAttribute("styleFilter", style);
        model.addAttribute("countryFilter", country);
        model.addAttribute("abvFilter", abv);
        model.addAttribute("ratingFilter", rating);
        model.addAttribute("sortBy", sortBy);
        model.addAttribute("tagID", tagID);

        int totalPages = beerPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages).boxed().collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        return "beers";
    }

    @PostMapping("/beers/add-beer")
    public String createBeer(@Valid @ModelAttribute Beer beer, @RequestParam("files") MultipartFile file,
                             @RequestParam("addTags") String tagInput, Principal principal) {

        controllerHelper.savePicture(beer, file);
        UserDetail user = userService.getByEmail(principal.getName());
        beer.setUserDetail(user);
        try {
            beerServices.createBeer(beer);
        } catch (DuplicateEntityException ex) {
            return String.format("redirect:/error?errorMessage=%s", ex.getMessage());
        }

        setBeerTags(beer, tagInput);
        return "redirect:/beers";
    }

    private void setBeerTags(Beer beer, String tagInput) {
        Set<String> inputTags = Arrays.stream(tagInput.split(" ")).collect(Collectors.toSet());
        List<Tag> allTags = populateTags();
        for (String newTag : inputTags) {
            Tag tempTag = allTags.stream().filter(tag -> tag.getName().equals(newTag)).findFirst().orElse(null);
            if (tempTag == null) {
                tempTag = new Tag();
                tempTag.setName(newTag);
                tempTag = tagService.createTag(tempTag);
            }
            beer.getTags().add(tempTag);
        }
        beerServices.updateBeer(beer);
    }

    @PostMapping("/beers/add-style")
    public String createStyle(@RequestParam(name = "fromPage", required = false) String beerID, @Valid @ModelAttribute Style style) {
        styleService.createStyle(style);
        if (beerID != null) {
            return "redirect:/beers/" + beerID;
        }
        return "redirect:/beers/";
    }


    @GetMapping("/beers/{id}")
    public String showBeer(@PathVariable int id, Model model, Principal principal) {
        Beer beer = null;
        try {
            beer = beerServices.getBeerById(id);
        } catch (EntityNotFoundException ex) {
            return String.format("redirect:/error?errorMessage=%s", ex.getMessage());
        }

        List<Rating> beerRatings = beer.getBeerRatings().stream().filter(rating -> rating.getUserDetail().getLogin().isEnabled()).collect(Collectors.toList());

        if (principal != null) {
            UserDetail user = userService.getByEmail(principal.getName());
            model.addAttribute("user", user);
            model.addAttribute("userRated", beerRatings.stream().anyMatch(rating ->
                    rating.getUserDetail().getId() == user.getId()));
        }

        BeerDto beerDto = new BeerDto();
        beerDto.setName(beer.getName());
        beerDto.setDescription(beer.getDescription());
        beerDto.setAbv(beer.getAbv());
        beerDto.setBreweryId(beer.getBrewery().getId());
        beerDto.setStyleId(beer.getStyle().getId());
        beerDto.setPictureURL(beer.getPicture());

        StringBuilder beerTags = new StringBuilder("");
        beer.getTags().forEach(tag -> beerTags.append(tag.getName()).append(" "));
        beerDto.setTags(beerTags.toString());

        model.addAttribute("beer", beer);
        model.addAttribute("ratings", beerRatings);
        model.addAttribute("newRating", new Rating());
        model.addAttribute("style", new Style());
        model.addAttribute("beerCreator", beer.getUserDetail());
        model.addAttribute("beerDTO", beerDto);
//        model.addAttribute("beerTags", beerTags.toString().replaceAll(" ", "# "));
        return "beer-page";
    }

    @PostMapping("/beers/{id}/update")
    public String updateBeer(@PathVariable int id, @ModelAttribute("beerDTO") BeerDto beerDto, @RequestParam("files") MultipartFile file) {

        Beer beerToUpdate = null;

        try {
            beerToUpdate = beerServices.getBeerById(id);
        } catch (EntityNotFoundException ex) {
            return String.format("redirect:/error?errorMessage=%s", ex.getMessage());
        }

        if (!file.isEmpty()) {
            controllerHelper.savePicture(beerToUpdate, file);
        }
        setBeerTags(beerToUpdate, beerDto.getTags());
        beerServices.updateBeer(dtoMapper.updateBeerFromDto(beerToUpdate, beerDto));
        return "redirect:/beers/{id}";
    }

    @PostMapping("/beers/{id}/rate-beer")
    public String rateBeer(@PathVariable int id, @ModelAttribute("newRating") Rating rating, Principal principal) {
        UserDetail user = userService.getByEmail(principal.getName());
        Beer beer = null;
        try {
            beer = beerServices.getBeerById(id);
        } catch (EntityNotFoundException ex) {
            return String.format("redirect:/error?errorMessage=%s", ex.getMessage());
        }
        ratingService.createRating(rating, user, beer);
        return "redirect:/beers/{id}";
    }

    @PostMapping("/beers/{id}/delete")
    public String deleteBeer(@PathVariable int id) {
        try {
            beerServices.deleteBeer(id);
        } catch (EntityNotFoundException ex) {
            return String.format("redirect:/error?errorMessage=%s", ex.getMessage());
        }
        return "redirect:/beers";
    }

}
