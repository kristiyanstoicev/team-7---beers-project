package com.example.beerTag.controllers;

import com.example.beerTag.entities.Brewery;
import com.example.beerTag.entities.Country;
import com.example.beerTag.helper.ControllerHelper;
import com.example.beerTag.models.BreweryDto;
import com.example.beerTag.models.DtoMapper;
import com.example.beerTag.repositories.BreweriesRepository;
import com.example.beerTag.services.BreweryService;
import com.example.beerTag.services.CountryService;
import com.example.beerTag.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
public class BreweriesController {

    private BreweryService breweryService;
    private CountryService countryService;
    private UserService userService;
    private ControllerHelper controllerHelper;
    private DtoMapper dtoMapper;

    @Autowired
    public BreweriesController(BreweryService breweryService, CountryService countryService,
                               UserService userService, ControllerHelper controllerHelper, DtoMapper dtoMapper) {
        this.breweryService = breweryService;
        this.countryService = countryService;
        this.userService = userService;
        this.controllerHelper = controllerHelper;
        this.dtoMapper = dtoMapper;
    }

    @ModelAttribute("countries")
    public List<Country> populateCountries() {
        return countryService.getAll();
    }

    @GetMapping("/breweries")
    public String showBreweries(@RequestParam(name = "name", defaultValue = "") String breweryName,
                                @RequestParam(name = "country", defaultValue = "") String country,
                                @RequestParam(name = "page", defaultValue = "1") int currentPage,
                                @RequestParam(name = "size", defaultValue = "6") int pageSize,
                                Model model, Principal principal) {
        Page<Brewery> breweryPage = controllerHelper.getItemsPaged(new Brewery(),
                PageRequest.of(currentPage - 1, pageSize),
                breweryName, country);

        if (principal != null) {
            model.addAttribute("user", userService.getByEmail(principal.getName()));
        }
        model.addAttribute("breweryPage", breweryPage);
        model.addAttribute("breweryDTO", new BreweryDto());
        model.addAttribute("nameFilter", breweryName);
        model.addAttribute("countryFilter", country);
        model.addAttribute("newBrewery", new Brewery());

        int totalPages = breweryPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages).boxed().collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        return "breweries";
    }


    @PostMapping("/breweries/add-brewery")
    public String createBrewery(@Valid @ModelAttribute BreweryDto newBrewery, @RequestParam("files") MultipartFile file) {
        controllerHelper.savePicture(newBrewery, file);
        breweryService.createBrewery(dtoMapper.fromDto(newBrewery));
        return "redirect:/breweries";
    }

    @PostMapping("/breweries/{id}/update")
    public String updateBrewery(@PathVariable int id, @ModelAttribute("breweryDTO") BreweryDto breweryDto, @ModelAttribute("newBrewery") Brewery brewery) {
        return null;
    }
}
