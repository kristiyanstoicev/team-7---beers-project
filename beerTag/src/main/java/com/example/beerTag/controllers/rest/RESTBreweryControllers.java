package com.example.beerTag.controllers.rest;

import com.example.beerTag.entities.UserDetail;
import com.example.beerTag.exceptions.DuplicateEntityException;
import com.example.beerTag.exceptions.EntityNotFoundException;
import com.example.beerTag.entities.Brewery;
import com.example.beerTag.models.BreweryDto;
import com.example.beerTag.models.DtoMapper;
import com.example.beerTag.services.BreweryService;
import com.example.beerTag.services.CountryService;
import com.example.beerTag.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/breweries")
public class RESTBreweryControllers {

    private BreweryService breweryService;
    private CountryService countryService;
    private UserService userService;
    private DtoMapper mapper;

    @Autowired
    public RESTBreweryControllers(BreweryService services, CountryService countryService, UserService userService, DtoMapper mapper) {
        this.breweryService = services;
        this.countryService = countryService;
        this.userService = userService;
        this.mapper = mapper;
    }

    @GetMapping
    public List<Brewery> getAll(@RequestParam(value = "name", defaultValue = "") String name,
                                @RequestParam(value = "country", defaultValue = "") String countryOrigin) {

        return breweryService.getAll(name, countryOrigin);
    }

    @GetMapping("/{id}")
    public Brewery getById(@PathVariable int id) {
        try {
            return breweryService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Brewery createBrewery(@RequestBody @Valid BreweryDto breweryDto,
                                 @RequestHeader(name = "Authorization") String userEmail) {
        try {
            UserDetail userDetail = userService.getByEmail(userEmail);
            Brewery newBrewery = mapper.fromDto(breweryDto);
            newBrewery.setName(breweryDto.getName());
            newBrewery.setCountry(countryService.getById(breweryDto.getCountryId()));
            return breweryService.createBrewery(newBrewery);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Brewery updateBrewery(@PathVariable int id, @RequestBody @Valid BreweryDto breweryDto,
                                 @RequestHeader(name = "Authorization") String userEmail) {
        try {
            UserDetail userDetail = userService.getByEmail(userEmail);
            Brewery newBrewery = breweryService.getById(id);
            newBrewery = mapper.updateBreweryFromDto(newBrewery, breweryDto);
            breweryService.updateBrewery(id, newBrewery);
            return newBrewery;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void deleteBrewery(@PathVariable int id,
                              @RequestHeader(name = "Authorization") String userEmail) {
        try {
            UserDetail userDetail = userService.getByEmail(userEmail);
            breweryService.deleteBrewery(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}

