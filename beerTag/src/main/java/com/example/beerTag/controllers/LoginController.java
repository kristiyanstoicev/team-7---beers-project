package com.example.beerTag.controllers;

import com.example.beerTag.models.UserDto;
import com.example.beerTag.services.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {

    CountryService countryService;

    @Autowired
    public LoginController(CountryService countryService) {
        this.countryService = countryService;
    }

    @GetMapping("/login")
    public String login(Model model) {

        // I need them for register form
        model.addAttribute("userDto", new UserDto());
        model.addAttribute("countries", countryService.getAll());

        return "login";
    }

    @GetMapping("/access-denied")
    public String showAccessDenied() {
        return "access-denied";
    }
}
