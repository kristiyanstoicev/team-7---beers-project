package com.example.beerTag.controllers.rest;


import com.example.beerTag.entities.Beer;
import com.example.beerTag.entities.UserDetail;
import com.example.beerTag.exceptions.DuplicateEntityException;
import com.example.beerTag.exceptions.EntityNotFoundException;
import com.example.beerTag.models.DtoMapper;
import com.example.beerTag.models.UserDto;
import com.example.beerTag.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class RESTUserControllers {


    private UserService services;
    private DtoMapper mapper;

    @Autowired
    public RESTUserControllers(UserService services, DtoMapper mapper) {
        this.services = services;
        this.mapper = mapper;
    }
//
//    @GetMapping
//    public List<UserDetail> getAll() {
//        return services.getAll();
//    }

    @GetMapping("/drankList")
    public List<Beer> getDrankList(@RequestHeader(name = "Authorization") String userEmail) {
        String typeOfList = "drank";
        UserDetail user = services.getByEmail(userEmail);
        return services.getList(user , typeOfList);
    }

    @GetMapping("/wishList")
    public List<Beer> getWishList(@RequestHeader(name = "Authorization") String userEmail) {
        String typeOfList = "wish";
        UserDetail user = services.getByEmail(userEmail);
        return services.getList(user , typeOfList);
    }

//    @GetMapping("/{email}")
//    public User getByEmail(@PathVariable String email) {
//        try {
//            return services.getUserByEmail(email);
//        } catch (EntityNotFoundException e) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
//        }
//    }

    @GetMapping("/{id}")
    public UserDetail getById(@PathVariable int id) {
        try {
            return services.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @PostMapping
    public UserDetail createUser(@Valid @RequestBody UserDto userDto) {
        try {
            UserDetail newUserDetail = mapper.fromDto(userDto);
             services.createUser(newUserDetail);
             return newUserDetail;

        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public UserDetail updateUser(@PathVariable int id, @RequestBody @Valid UserDto userDto) {
        try {
            UserDetail updateUserDetail = services.getById(id);
            updateUserDetail = mapper.updateUserFromDto(userDto, updateUserDetail);
            return services.updateUser(updateUserDetail);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable int id) {
        try {
            services.deleteUser(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/addToDrank/{beerId}")
    public void addBeerToDrankList( @RequestHeader(name = "Authorization") String userEmail, @PathVariable int beerId) {
        try {
            UserDetail user = services.getByEmail(userEmail);
            services.addBeerToWishList(user, beerId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping("/addToWish/{beerId}")
    public void addBeerToWishList( @RequestHeader(name = "Authorization") String userEmail, @PathVariable int beerId) {
        try {
            UserDetail user = services.getByEmail(userEmail);
            services.addBeerToWishList(user, beerId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/deleteFromLists/{beerId}")
    public void deleteBeerFromList( @RequestHeader(name = "Authorization") String userEmail, @PathVariable int beerId) {
        try {
            UserDetail user = services.getByEmail(userEmail);
            services.addBeerToWishList(user, beerId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


}
