package com.example.beerTag.controllers;

import com.example.beerTag.entities.UserDetail;
import com.example.beerTag.models.DtoMapper;
import com.example.beerTag.models.UserDto;
import com.example.beerTag.services.CountryService;
import com.example.beerTag.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class RegistrationController {
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;
    private UserService userService;
    private CountryService countryService;
    private DtoMapper mapper;


    @Autowired
    public RegistrationController(UserDetailsManager userDetailsManager, PasswordEncoder passwordEncoder,
                                  UserService userService, CountryService countryService, DtoMapper mapper) {
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
        this.countryService = countryService;
        this.mapper = mapper;
    }

//    @GetMapping("/register")
//    public String showRegisterPage(Model model) {
//        model.addAttribute("userDto", new UserDto());
//        model.addAttribute("countries", countryService.getAll());
//        return "register";
//    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute UserDto userDto,
                               BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {

            return "login";
        }

        if (userDetailsManager.userExists(userDto.getEmail())) {
            model.addAttribute("error", "User with the same username already exists!");
            return "login";
        }

        if (!userDto.getPassword().equals(userDto.getPasswordConfirmation())) {
            model.addAttribute("error", "Password does't match!");
            return "login";
        }

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        userDto.getEmail(),
                        passwordEncoder.encode(userDto.getPassword()),
                        authorities);

        userDetailsManager.createUser(newUser);
        UserDetail userDetail = mapper.fromDto(userDto);

        userService.createUser(userDetail);

        return "register-confirmation";
    }


    @GetMapping("/register-confirmation")
    public String showRegisterConfirmation() {
        return "register-confirmation";
    }
}