package com.example.beerTag.controllers.rest;

import com.example.beerTag.entities.Beer;
import com.example.beerTag.entities.Style;
import com.example.beerTag.entities.UserDetail;
import com.example.beerTag.exceptions.DuplicateEntityException;
import com.example.beerTag.exceptions.EntityNotFoundException;
import com.example.beerTag.exceptions.InvalidOperationException;
import com.example.beerTag.models.StyleDto;
import com.example.beerTag.services.StyleService;
import com.example.beerTag.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/styles")
public class RESTStyleControllers {

    private StyleService services;
    private UserService userService;

    @Autowired
    public RESTStyleControllers(StyleService services, UserService userService) {
        this.services = services;
        this.userService = userService;
    }

    @GetMapping
    public List<Style> getAll(@RequestParam(value = "name", defaultValue = "") String name) {
        return services.getAll(name);
    }


    @GetMapping("/{id}")
    public Style getById(@PathVariable int id) {
        try {
            return services.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

//    @GetMapping("/{styleId}/beers")
//    public List<Beer> getBeersByStyle(@PathVariable int styleId) {
//        try {
//            return services.getBeersByStyle(styleId);
//        } catch (EntityNotFoundException e) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
//        }
//    }

    @PostMapping
    public Style createStyle(@RequestBody StyleDto styleDto, @RequestHeader(name = "Authorization")
            String authorization) {
        try {
            UserDetail requestUserDetail = userService.getByEmail(authorization);
            Style style = new Style();
            style.setName(styleDto.getName());
            style.setDescription(styleDto.getDescription());
//            style.setCreatedByUserDetailId(requestUserDetail);
            services.createStyle(style);
            return style;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Style updateStyle(@PathVariable int id, @RequestBody StyleDto updateStyleDto,
                             @RequestHeader(name = "Authorization")
                                     String authorization) {
        try {
            UserDetail userDetail = userService.getByEmail(authorization);
            Style styleToUpdate = services.getById(id);
            styleToUpdate.setDescription(updateStyleDto.getDescription());
            services.updateStyle(styleToUpdate);
            return styleToUpdate;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void deleteStyle(@PathVariable int id,
                            @RequestHeader(name = "Authorization")
                                    String authorization) {
        try {
            Style styleToDelete = services.getById(id);
            UserDetail userDetail = userService.getByEmail(authorization);
            services.deleteStyle(styleToDelete);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

    }

}
