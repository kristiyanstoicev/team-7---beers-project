package com.example.beerTag.controllers;

import com.example.beerTag.entities.UserDetail;
import com.example.beerTag.exceptions.DuplicateEntityException;
import com.example.beerTag.exceptions.EntityNotFoundException;
import com.example.beerTag.models.DtoMapper;
import com.example.beerTag.models.UserDto;
import com.example.beerTag.repositories.LoginRepository;
import com.example.beerTag.services.CountryService;
import com.example.beerTag.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;
import java.util.List;

@Controller
public class UsersController {

    private UserService userService;
    private CountryService countryService;
    private DtoMapper mapper;
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;
    private LoginRepository loginRepository;

    @Autowired
    public UsersController(UserService userService, CountryService countryService, DtoMapper mapper, UserDetailsManager userDetailsManager, PasswordEncoder passwordEncoder, LoginRepository loginRepository) {
        this.userService = userService;
        this.countryService = countryService;
        this.mapper = mapper;
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.loginRepository = loginRepository;
    }

    @GetMapping("/users")
    public String getAllUsers(@RequestParam(name = "firstName", defaultValue = "") String firstName,
                              @RequestParam(name = "lastName", defaultValue = "") String lastName,
                              @RequestParam(name = "sortBy", defaultValue = "") String sortBy,
                              Principal principal, Model model) {

        UserDetail userDetail = userService.getByEmail(principal.getName());
        model.addAttribute("users", userService.getAll(firstName, lastName, sortBy));
        model.addAttribute("userDetail", userDetail);
        model.addAttribute("countries", countryService.getAll());

        return "users";
    }

    @GetMapping("/users/{userId}")
    public String userDetailsPage(Principal principal, Model model, @PathVariable int userId) {

        UserDetail userDetail = null;
        try {
            userDetail = userService.getById(userId);
        } catch (EntityNotFoundException ex) {
            return String.format("redirect:/error?errorMessage=%s", ex.getMessage());
        }

        UserDetail userPrincipal = userService.getByEmail(principal.getName());
        model.addAttribute("topThreeBeers", userService.getTopThreeRated(userDetail));
        model.addAttribute("userDetail", userDetail);
        model.addAttribute("userPrincipal", userPrincipal);
        return "user-detail";
    }

    @GetMapping("/updateUser/{userId}")
    public String showUpdateUserPage(Model model, @PathVariable int userId) {

        model.addAttribute("userDto", new UserDto());
        try {
            model.addAttribute("user", userService.getById(userId));
        } catch (EntityNotFoundException ex) {
            return String.format("redirect:/error?errorMessage=%s", ex.getMessage());
        }
        model.addAttribute("countries", countryService.getAll());
        return "updateUser";
    }

    @PostMapping("/updateUser/{userId}")
    public String updateUserDetails(@Valid @ModelAttribute UserDto userDto, @PathVariable int userId,
                                    @RequestParam("files") MultipartFile file) {

        UserDetail currentUser= null;
        try {
            currentUser = userService.getById(userId);
        } catch (EntityNotFoundException ex) {
            return String.format("redirect:/error?errorMessage=%s", ex.getMessage());
        }

        //changing the password in user through security
        if (!userDto.getPassword().isEmpty()) {
            List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
            org.springframework.security.core.userdetails.User toUpdateUser =
                    new org.springframework.security.core.userdetails.User(
                            currentUser.getEmail(),
                            passwordEncoder.encode(userDto.getPassword()),
                            authorities);
            userDetailsManager.updateUser(toUpdateUser);
        }

        currentUser.setFirstName(userDto.getFirstName());
        currentUser.setLastName(userDto.getLastName());
        currentUser.setCountry(countryService.getById(userDto.getCountryId()));

        try {
            String path = new File("src/main/media/uploads").getAbsolutePath();
            Path fileNameAdnPath = Paths.get(path, file.getOriginalFilename());
            Files.write(fileNameAdnPath, file.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (!file.isEmpty()) {
            currentUser.setPhoto("/media/uploads/" + file.getOriginalFilename());
        }

        userService.updateUser(currentUser);

        String idString = String.valueOf(currentUser.getId());
        return "redirect:/users/" + idString;
    }


    @GetMapping("/users/addToDrank/{id}")
    public String addBeerToDrankList(@PathVariable int id, Principal principal) {
        try {
            userService.addBeerToDrankList(userService.getByEmail(principal.getName()), id);
        } catch (EntityNotFoundException | DuplicateEntityException ex) {
            return String.format("redirect:/error?errorMessage=%s", ex.getMessage());
        }
        return "redirect:/beers";
    }

    @GetMapping("/users/addToWish/{id}")
    public String addBeerToWishList(@PathVariable int id, Principal principal) {
        try {
            userService.addBeerToWishList(userService.getByEmail(principal.getName()), id);
        } catch (EntityNotFoundException | DuplicateEntityException ex) {
            return String.format("redirect:/error?errorMessage=%s", ex.getMessage());
        }
        return "redirect:/beers";
    }

    @GetMapping("/users/removeFromList/{beerId}")
    public String deleteBeerFromList(@PathVariable int beerId, Principal principal) {

        try {
            userService.deleteBeerFromList(userService.getByEmail(principal.getName()), beerId);
        } catch (EntityNotFoundException ex) {
            return String.format("redirect:/error?errorMessage=%s", ex.getMessage());
        }

        return "redirect:/beers";
    }

    @PostMapping("/users/delete/{userId}")
    public String deleteUser(@PathVariable int userId, Principal principal, HttpServletRequest request) {

        try {
            userService.deleteUser(userId);
        } catch (EntityNotFoundException ex) {
            return String.format("redirect:/error?errorMessage=%s", ex.getMessage());
        }

        UserDetail userPrincipal = userService.getByEmail(principal.getName());

        if (userId == userPrincipal.getId()) {
            request.getSession().invalidate();
            return "redirect:/";
        }

        return "redirect:/users";
    }

}
