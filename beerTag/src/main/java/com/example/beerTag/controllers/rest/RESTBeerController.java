package com.example.beerTag.controllers.rest;

import com.example.beerTag.entities.UserDetail;
import com.example.beerTag.exceptions.DuplicateEntityException;
import com.example.beerTag.exceptions.EntityNotFoundException;
import com.example.beerTag.entities.Beer;
import com.example.beerTag.models.BeerDto;
import com.example.beerTag.models.DtoMapper;
import com.example.beerTag.services.BeerServices;
import com.example.beerTag.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/beers")
public class RESTBeerController {

    private BeerServices beerServices;
    private UserService userService;
    private DtoMapper mapper;

    @Autowired
    public RESTBeerController(BeerServices beerServices, UserService userService, DtoMapper mapper) {
        this.beerServices = beerServices;
        this.userService = userService;
        this.mapper = mapper;
    }

    @GetMapping
    public List<Beer> getAll(@RequestParam(value = "name", defaultValue = "") String name,
                             @RequestParam(value = "style", defaultValue = "") String styleName,
                             @RequestParam(value = "brewery", defaultValue = "") String breweryName,
                             @RequestParam(value = "country", defaultValue = "") String countryName,
                             @RequestParam(value = "abv", defaultValue = "") String abv,
                             @RequestParam(value = "rating", defaultValue = "") String rating,
                             @RequestParam(value = "sortBy", defaultValue = "") String sortBy,
                             @RequestParam(value = "tagID", defaultValue = "") String tagID) {

        return beerServices.getAll(name, breweryName, countryName, styleName, abv, rating, sortBy, tagID);
    }

    @GetMapping("/{id}")
    public Beer getById(@PathVariable int id) {
        try {
            return beerServices.getBeerById(id);
        } catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @PostMapping
    public Beer createBeer(@Valid @RequestBody BeerDto beerDto,
                           @RequestHeader(name = "Authorization") String userEmail) {
        try {
            UserDetail userDetail = userService.getByEmail(userEmail);
            Beer beer = mapper.fromDto(beerDto);
            beerServices.createBeer(beer);
            return beer;
        } catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        } catch (DuplicateEntityException ex) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, ex.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Beer updateBeer(@PathVariable int id, @RequestBody BeerDto beerDto,
                           @RequestHeader(name = "Authorization") String userEmail) {
        try {
            UserDetail userDetail = userService.getByEmail(userEmail);
            Beer beer = beerServices.getBeerById(id);
            beer = mapper.updateBeerFromDto(beer, beerDto);
            beerServices.updateBeer(beer);
            return beer;
        } catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void deleteBeer(@PathVariable int id,
                           @RequestHeader(name = "Authorization") String userEmail) {
        try {
            UserDetail userDetail = userService.getByEmail(userEmail);
            beerServices.deleteBeer(id);
        } catch (EntityNotFoundException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ex.getMessage());
        }
    }
}
