package com.example.beerTag.controllers;

import com.example.beerTag.entities.UserDetail;
import com.example.beerTag.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Controller("/")
public class HomeController {
    private UserService userService;

    @Autowired
    public HomeController(UserService userService) {
        this.userService = userService;
    }

//    @GetMapping
//    public String showHomePage( ) {
//        return "/index";
//    }

    @GetMapping
    public String showHomePage( Principal principal, Model model) {
        if (principal != null) {
            UserDetail userDetail = userService.getByEmail(principal.getName());
            model.addAttribute("userDetail", userDetail);
        }
        return "index";
    }



}
