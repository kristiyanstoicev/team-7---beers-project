package com.example.beerTag.exceptions;

public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException(String itemType, int id) {
        this(itemType,"id", String.valueOf(id));
    }

    public EntityNotFoundException(String itemType, String attribute, String value){
        super(String.format("%s with %s %s not found",itemType,attribute,value));
    }

    public EntityNotFoundException(String message) {
        super(message);
    }
}
