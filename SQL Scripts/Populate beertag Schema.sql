INSERT INTO `countries` (`country_id`, `country_name`, `is_deleted`) VALUES
	(1, 'randomes', '0'),
	(2, 'Afghanistan', '0'),
	(3, 'Albania', '0'),
	(4, 'Algeria', '0'),
	(5, 'Andorra', '0'),
	(6, 'Angola', '0'),
	(7, 'Antigua & Deps', '0'),
	(8, 'Argentina', '0'),
	(9, 'Armenia', '0'),
	(10, 'Australia', '0'),
	(11, 'Austria', '0'),
	(12, 'Azerbaijan', '0'),
	(13, 'Bahamas', '0'),
	(14, 'Bahrain', '0'),
	(15, 'Bangladesh', '0'),
	(16, 'Barbados', '0'),
	(17, 'Belarus', '0'),
	(18, 'Belgium', '0'),
	(19, 'Belize', '0'),
	(20, 'Benin', '0'),
	(21, 'Bhutan', '0'),
	(22, 'Bolivia', '0'),
	(23, 'Bosnia Herzegovina', '0'),
	(24, 'Botswana', '0'),
	(25, 'Brazil', '0'),
	(26, 'Brunei', '0'),
	(27, 'Bulgaria', '0'),
	(28, 'Burkina', '0'),
	(29, 'Burundi', '0'),
	(30, 'Cambodia', '0'),
	(31, 'Cameroon', '0'),
	(32, 'Canada', '0'),
	(33, 'Cape Verde', '0'),
	(34, 'Central African Rep', '0'),
	(35, 'Chad', '0'),
	(36, 'Chile', '0'),
	(37, 'China', '0'),
	(38, 'Colombia', '0'),
	(39, 'Comoros', '0'),
	(40, 'Congo', '0'),
	(41, 'Congo {Democratic Rep}', '0'),
	(42, 'Costa Rica', '0'),
	(43, 'Croatia', '0'),
	(44, 'Cuba', '0'),
	(45, 'Cyprus', '0'),
	(46, 'Czech Republic', '0'),
	(47, 'Denmark', '0'),
	(48, 'Djibouti', '0'),
	(49, 'Dominica', '0'),
	(50, 'Dominican Republic', '0'),
	(51, 'East Timor', '0'),
	(52, 'Ecuador', '0'),
	(53, 'Egypt', '0'),
	(54, 'El Salvador', '0'),
	(55, 'Equatorial Guinea', '0'),
	(56, 'Eritrea', '0'),
	(57, 'Estonia', '0'),
	(58, 'Ethiopia', '0'),
	(59, 'Fiji', '0'),
	(60, 'Finland', '0'),
	(61, 'France', '0'),
	(62, 'Gabon', '0'),
	(63, 'Gambia', '0'),
	(64, 'Georgia', '0'),
	(65, 'Germany', '0'),
	(66, 'Ghana', '0'),
	(67, 'Greece', '0'),
	(68, 'Grenada', '0'),
	(69, 'Guatemala', '0'),
	(70, 'Guinea', '0'),
	(71, 'Guinea-Bissau', '0'),
	(72, 'Guyana', '0'),
	(73, 'Haiti', '0'),
	(74, 'Honduras', '0'),
	(75, 'Hungary', '0'),
	(76, 'Iceland', '0'),
	(77, 'India', '0'),
	(78, 'Indonesia', '0'),
	(79, 'Iran', '0'),
	(80, 'Iraq', '0'),
	(81, 'Ireland {Republic}', '0'),
	(82, 'Israel', '0'),
	(83, 'Italy', '0'),
	(84, 'Ivory Coast', '0'),
	(85, 'Jamaica', '0'),
	(86, 'Japan', '0'),
	(87, 'Jordan', '0'),
	(88, 'Kazakhstan', '0'),
	(89, 'Kenya', '0'),
	(90, 'Kiribati', '0'),
	(91, 'Korea North', '0'),
	(92, 'Korea South', '0'),
	(93, 'Kosovo', '0'),
	(94, 'Kuwait', '0'),
	(95, 'Kyrgyzstan', '0'),
	(96, 'Laos', '0'),
	(97, 'Latvia', '0'),
	(98, 'Lebanon', '0'),
	(99, 'Lesotho', '0'),
	(100, 'Liberia', '0'),
	(101, 'Libya', '0'),
	(102, 'Liechtenstein', '0'),
	(103, 'Lithuania', '0'),
	(104, 'Luxembourg', '0'),
	(105, 'Macedonia', '0'),
	(106, 'Madagascar', '0'),
	(107, 'Malawi', '0'),
	(108, 'Malaysia', '0'),
	(109, 'Maldives', '0'),
	(110, 'Mali', '0'),
	(111, 'Malta', '0'),
	(112, 'Marshall Islands', '0'),
	(113, 'Mauritania', '0'),
	(114, 'Mauritius', '0'),
	(115, 'Mexico', '0'),
	(116, 'Micronesia', '0'),
	(117, 'Moldova', '0'),
	(118, 'Monaco', '0'),
	(119, 'Mongolia', '0'),
	(120, 'Montenegro', '0'),
	(121, 'Morocco', '0'),
	(122, 'Mozambique', '0'),
	(123, 'Myanmar, {Burma}', '0'),
	(124, 'Namibia', '0'),
	(125, 'Nauru', '0'),
	(126, 'Nepal', '0'),
	(127, 'Netherlands', '0'),
	(128, 'New Zealand', '0'),
	(129, 'Nicaragua', '0'),
	(130, 'Niger', '0'),
	(131, 'Nigeria', '0'),
	(132, 'Norway', '0'),
	(133, 'Oman', '0'),
	(134, 'Pakistan', '0'),
	(135, 'Palau', '0'),
	(136, 'Panama', '0'),
	(137, 'Papua New Guinea', '0'),
	(138, 'Paraguay', '0'),
	(139, 'Peru', '0'),
	(140, 'Philippines', '0'),
	(141, 'Poland', '0'),
	(142, 'Portugal', '0'),
	(143, 'Qatar', '0'),
	(144, 'Romania', '0'),
	(145, 'Russian Federation', '0'),
	(146, 'Rwanda', '0'),
	(147, 'St Kitts & Nevis', '0'),
	(148, 'St Lucia', '0'),
	(149, 'Saint Vincent & the Grenadines', '0'),
	(150, 'Samoa', '0'),
	(151, 'San Marino', '0'),
	(152, 'Sao Tome & Principe', '0'),
	(153, 'Saudi Arabia', '0'),
	(154, 'Senegal', '0'),
	(155, 'Serbia', '0'),
	(156, 'Seychelles', '0'),
	(157, 'Sierra Leone', '0'),
	(158, 'Singapore', '0'),
	(159, 'Slovakia', '0'),
	(160, 'Slovenia', '0'),
	(161, 'Solomon Islands', '0'),
	(162, 'Somalia', '0'),
	(163, 'South Africa', '0'),
	(164, 'South Sudan', '0'),
	(165, 'Spain', '0'),
	(166, 'Sri Lanka', '0'),
	(167, 'Sudan', '0'),
	(168, 'Suriname', '0'),
	(169, 'Swaziland', '0'),
	(170, 'Sweden', '0'),
	(171, 'Switzerland', '0'),
	(172, 'Syria', '0'),
	(173, 'Taiwan', '0'),
	(174, 'Tajikistan', '0'),
	(175, 'Tanzania', '0'),
	(176, 'Thailand', '0'),
	(177, 'Togo', '0'),
	(178, 'Tonga', '0'),
	(179, 'Trinidad & Tobago', '0'),
	(180, 'Tunisia', '0'),
	(181, 'Turkey', '0'),
	(182, 'Turkmenistan', '0'),
	(183, 'Tuvalu', '0'),
	(184, 'Uganda', '0'),
	(185, 'Ukraine', '0'),
	(186, 'United Arab Emirates', '0'),
	(187, 'United Kingdom', '0'),
	(188, 'United States', '0'),
	(189, 'Uruguay', '0'),
	(190, 'Uzbekistan', '0'),
	(191, 'Vanuatu', '0'),
	(192, 'Vatican City', '0'),
	(193, 'Venezuela', '0'),
	(194, 'Vietnam', '0'),
	(195, 'Yemen', '0'),
	(196, 'Zambia', '0'),
	(197, 'Zimbabwe', '0');


INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Bhutan Brewery', '2018-09-27 09:47:20', 77, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Azerbaijan Brewery', '2019-10-11 02:01:52', 83, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Afghanistan Brewery', '2019-02-25 02:52:44', 143, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Bahrain Brewery', '2019-10-01 20:03:54', 60, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Cameroon Brewery', '2018-05-16 17:57:50', 80, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Albania Brewery', '2018-12-25 09:20:48', 5, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Afghanistan Brewery', '2019-02-13 21:53:48', 26, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Belarus Brewery', '2018-05-23 11:42:54', 17, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Benin Brewery', '2018-01-25 03:32:12', 65, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Cabo_Verde Brewery', '2019-08-11 06:12:45', 18, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Argentina Brewery', '2018-12-08 12:34:27', 40, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Angola Brewery', '2018-03-31 01:17:26', 48, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Central_African_Republic Brewery', '2019-09-26 23:22:18', 112, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Belgium Brewery', '2018-04-02 16:50:12', 62, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Algeria Brewery', '2018-12-18 11:30:33', 128, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Colombia Brewery', '2019-12-02 05:38:27', 35, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Chile Brewery', '2018-04-22 05:46:52', 21, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Cameroon Brewery', '2018-04-09 23:31:13', 11, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Congo Brewery', '2019-01-25 00:16:29', 60, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Andorra Brewery', '2019-10-01 20:03:54', 2, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Brunei Brewery', '2018-06-10 07:00:54', 8, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Bahrain Brewery', '2019-05-01 12:07:33', 57, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Chile Brewery', '2019-10-11 02:01:52', 18, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Cote_d_Ivoire Brewery', '2019-03-03 15:54:52', 8, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Bolivia Brewery', '2019-09-29 21:50:52', 57, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Belarus Brewery', '2019-10-30 01:03:06', 80, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Burundi Brewery', '2019-06-18 10:50:06', 23, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Congo Brewery', '2018-02-27 08:20:18', 146, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Benin Brewery', '2019-10-07 20:13:58', 16, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Colombia Brewery', '2018-08-15 06:37:37', 106, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Comoros Brewery', '2018-12-23 17:17:52', 68, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Bhutan Brewery', '2018-04-29 17:32:01', 123, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Armenia Brewery', '2018-12-01 03:19:10', 27, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Brazil Brewery', '2018-07-27 09:43:19', 47, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Bolivia Brewery', '2019-11-24 05:08:44', 62, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Cambodia Brewery', '2019-02-25 10:26:05', 50, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Cote_d_Ivoire Brewery', '2019-10-26 17:24:57', 150, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Andorra Brewery', '2018-05-18 16:56:23', 146, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Azerbaijan Brewery', '2018-03-31 01:17:26', 144, '/media/uploads/brewery-example.jpg', 0);
INSERT INTO beertag.breweries (brewery_name, date_created, country_id, brewery_picture, is_deleted) VALUES ('Chad Brewery', '2019-10-01 20:03:54', 121, '/media/uploads/brewery-example.jpg', 0);

INSERT INTO beertag.users (username, password, enabled) VALUES ('den@abv.bg', '$2a$10$Y6RUerPix0kklZnY5roWluJ34tvmDLxsrqYJh/eBLQY/PXA51uzPm', 1);
INSERT INTO beertag.users (username, password, enabled) VALUES ('dimityr@gmail.com', '$2a$10$p9IDwYThbD/Zk6brLcnXBOFp./4ZsOA59kUfN97B3ZbxJCugnJGAu', 1);
INSERT INTO beertag.users (username, password, enabled) VALUES ('gavi@gmail.com', '$2a$10$9.wH7qTAZw8r.jViixqLtOy1tJE5GpzFLjQc7nCHu41jeop2K92Y6', 1);
INSERT INTO beertag.users (username, password, enabled) VALUES ('gena@gmail.com', '$2a$10$pJpbpgiOh6shixjhM7q3O.0Vv0e/hSAo.wHVGTB6.s62hYPJ2YdMO', 1);
INSERT INTO beertag.users (username, password, enabled) VALUES ('ivan@gmail.com', '$2a$10$FVP2mmVIXJWRG9SdiJlTneBoBRQGSa8wkfITlOpXCjsDPcU0K5JtO', 1);
INSERT INTO beertag.users (username, password, enabled) VALUES ('kris@gmail.com', '$2a$10$rXMNU/neitpB5ap654vs9Oll6JtiEWcXCqyVmp/clpR7k4T9gkZUa', 1);
INSERT INTO beertag.users (username, password, enabled) VALUES ('petyr@gmail.com', '$2a$10$QCq/8Y9SxSJrUD/KMPLhjeIEAcrc7aOqxqOo16P3pWxYE3d5PPhjS', 1);
INSERT INTO beertag.users (username, password, enabled) VALUES ('stan@gmail.com', '$2a$10$5ZNMkOA633OzHzdghEk2wO/NyLTCmAnuNdBb9LG6YiwxJGUKYUXbi', 1);
INSERT INTO beertag.users (username, password, enabled) VALUES ('test@gmail.com', '$2a$10$QxHZPa6O7VN/wTSKdVReluybCYwziV4DHSZgT3U9Q1U4T79XR4SIi', 1);

INSERT INTO beertag.users_details (user_id, first_name, last_name, email, country_id, profile_picture, login_username) VALUES (1, 'test', 'test', 'test@gmail.com', 47, '/media/uploads/erik-mclean-TNjdgCBRMeU-unsplash.jpg', 'test@gmail.com');
INSERT INTO beertag.users_details (user_id, first_name, last_name, email, country_id, profile_picture, login_username) VALUES (2, 'Ivan', ' El Megraby', 'ivan@gmail.com', 17, '/media/uploads/default-photo.jpg', 'ivan@gmail.com');
INSERT INTO beertag.users_details (user_id, first_name, last_name, email, country_id, profile_picture, login_username) VALUES (3, 'Petyr', 'Dimitrov', 'petyr@gmail.com', 20, '/media/uploads/astronomy-1867616_1920.jpg', 'petyr@gmail.com');
INSERT INTO beertag.users_details (user_id, first_name, last_name, email, country_id, profile_picture, login_username) VALUES (4, 'Kristiyan', 'Stoitsev', 'kris@gmail.com', 27, '/media/uploads/default-photo.jpg', 'kris@gmail.com');
INSERT INTO beertag.users_details (user_id, first_name, last_name, email, country_id, profile_picture, login_username) VALUES (5, 'Dimityr', 'Kur', 'dimityr@gmail.com', 59, '/media/uploads/rick-and-morty-wallpaper.jpg', 'dimityr@gmail.com');
INSERT INTO beertag.users_details (user_id, first_name, last_name, email, country_id, profile_picture, login_username) VALUES (6, 'Stan', 'Ivanov', 'stan@gmail.com', 73, '/media/uploads/elisa-coluccia-v8iWQlz5wtc-unsplash.jpg', 'stan@gmail.com');
INSERT INTO beertag.users_details (user_id, first_name, last_name, email, country_id, profile_picture, login_username) VALUES (7, 'Gavril', 'S.123', 'gavi@gmail.com', 62, '/media/uploads/nature-2609647_1920.jpg', 'gavi@gmail.com');
INSERT INTO beertag.users_details (user_id, first_name, last_name, email, country_id, profile_picture, login_username) VALUES (8, 'Genata', 'Evgenata', 'gena@gmail.com', 189, '/media/uploads/foxsnow.jpg', 'gena@gmail.com');
INSERT INTO beertag.users_details (user_id, first_name, last_name, email, country_id, profile_picture, login_username) VALUES (9, 'Denitza', 'Ruseva', 'den@abv.bg', 73, '/media/uploads/james-donovan-kFHz9Xh3PPU-unsplash.jpg', 'den@abv.bg');

INSERT INTO beertag.authorities (username, authority) VALUES ('den@abv.bg', 'ROLE_USER');
INSERT INTO beertag.authorities (username, authority) VALUES ('dimityr@gmail.com', 'ROLE_USER');
INSERT INTO beertag.authorities (username, authority) VALUES ('gavi@gmail.com', 'ROLE_USER');
INSERT INTO beertag.authorities (username, authority) VALUES ('gena@gmail.com', 'ROLE_USER');
INSERT INTO beertag.authorities (username, authority) VALUES ('ivan@gmail.com', 'ROLE_USER');
INSERT INTO beertag.authorities (username, authority) VALUES ('kris@gmail.com', 'ROLE_USER');
INSERT INTO beertag.authorities (username, authority) VALUES ('petyr@gmail.com', 'ROLE_USER');
INSERT INTO beertag.authorities (username, authority) VALUES ('stan@gmail.com', 'ROLE_USER');
INSERT INTO beertag.authorities (username, authority) VALUES ('test@gmail.com', 'ROLE_USER');

INSERT INTO beertag.status (status_id, status_name) VALUES (1, 'drank');
INSERT INTO beertag.status (status_id, status_name) VALUES (2, 'wish');

INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Dark Lagers', 'description', 0, '2019-02-14 06:09:13');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Scotch Ale/Wee Heavy', 'description', 0, '2018-10-02 16:05:08');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Belgian-Style Fruit Lambic', 'description', 0, '2019-06-28 09:11:08');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Bocks', 'description', 0, '2018-03-31 01:17:26');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('German-Style Helles', 'description', 0, '2019-04-11 15:10:27');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('German-Style Altbier', 'description', 0, '2019-05-24 23:41:27');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('California Common', 'description', 0, '2019-06-01 19:37:28');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Baltic-Style Porter', 'description', 0, '2019-03-26 09:48:52');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('American Imperial Red Ale', 'description', 0, '2018-03-19 22:57:03');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('American Brett', 'description', 0, '2019-03-17 03:37:40');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Belgian-Style Lambic/Gueuze', 'description', 0, '2019-06-29 01:05:21');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Belgian-Style Quadrupel', 'description', 0, '2018-01-19 09:26:49');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Strong Ales', 'description', 0, '2019-07-23 05:42:45');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Irish-Style Dry Stout', 'description', 0, '2019-04-28 16:02:25');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('American Brett', 'description', 0, '2018-12-30 11:38:10');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('American Cream Ale', 'description', 0, '2019-05-29 12:42:10');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('American Amber Ale', 'description', 0, '2018-07-02 02:29:13');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('American Imperial Stout', 'description', 0, '2018-07-04 12:29:06');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('American Amber Ale', 'description', 0, '2019-11-24 05:08:44');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('British-Style Barley Wine Ale', 'description', 0, '2019-05-29 12:42:10');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('German-Style Hefeweizen', 'description', 0, '2018-10-04 00:37:17');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('American-Style Wheat Wine Ale', 'description', 0, '2018-05-02 00:02:02');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('German-Style Doppelbock', 'description', 0, '2019-07-28 06:16:37');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('American Wheat', 'description', 0, '2018-09-27 09:47:20');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('American Imperial Stout', 'description', 0, '2018-12-20 03:30:49');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Belgian-Style Flanders', 'description', 0, '2018-10-04 00:37:17');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Belgian-Style Lambic/Gueuze', 'description', 0, '2018-05-16 17:57:50');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('American IPA', 'description', 0, '2019-01-14 17:59:48');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Belgian-Style Blonde Ale', 'description', 0, '2019-05-30 02:29:18');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Robust Porter', 'description', 0, '2018-12-23 17:17:52');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Belgian-Style Golden Strong Ale', 'description', 0, '2019-08-11 06:12:45');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Belgian Styles', 'description', 0, '2018-05-29 14:01:08');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Rye Beer', 'description', 0, '2019-03-17 19:57:24');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('American Brett', 'description', 0, '2019-04-26 02:20:34');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('California Common', 'description', 0, '2018-05-19 11:19:03');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Bocks', 'description', 0, '2018-11-18 01:24:33');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('American Amber Ale', 'description', 0, '2019-01-06 11:44:43');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Smoke Porter', 'description', 0, '2018-09-11 22:28:12');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Gluten-Free Craft Beer', 'description', 0, '2018-08-23 01:06:22');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('American Barley Wine', 'description', 0, '2019-04-04 05:38:03');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('American IPA', 'description', 0, '2018-12-20 06:23:44');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('German-Style Altbier', 'description', 0, '2018-02-25 17:25:53');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Belgian-Style Pale Ale', 'description', 0, '2019-12-14 12:44:31');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('German-Style Marzen / Oktoberfest', 'description', 0, '2018-11-18 01:24:33');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('German-Style Weizenbock', 'description', 0, '2018-07-28 05:21:50');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Barrel-Aged Beer', 'description', 0, '2018-09-27 05:25:44');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Honey Beer', 'description', 0, '2019-05-23 21:35:21');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('California Common', 'description', 0, '2018-01-21 21:45:17');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Bocks', 'description', 0, '2019-04-26 02:04:05');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('German-Style Doppelbock', 'description', 0, '2018-02-27 08:20:18');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('American Amber Lager', 'description', 0, '2019-06-28 09:11:08');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('American Pale Ale', 'description', 0, '2018-02-12 04:20:19');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Baltic-Style Porter', 'description', 0, '2019-04-26 02:20:34');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Barrel-Aged Beer', 'description', 0, '2018-10-02 16:05:08');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Belgian-Style Tripel', 'description', 0, '2019-04-26 21:29:37');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('American Black Ale', 'description', 0, '2019-02-14 06:09:13');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('American Imperial Red Ale', 'description', 0, '2018-07-28 05:21:50');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Belgian-Style Golden Strong Ale', 'description', 0, '2018-06-03 10:20:21');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Belgian-Style Golden Strong Ale', 'description', 0, '2018-05-19 11:19:03');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Robust Porter', 'description', 0, '2018-11-14 12:34:46');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Pilseners and Pale Lagers', 'description', 0, '2019-05-24 23:41:27');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('American IPA', 'description', 0, '2019-05-30 02:29:18');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Belgian Styles', 'description', 0, '2018-06-11 13:03:36');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Hybrid Beers', 'description', 0, '2018-12-15 20:41:52');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('American Amber Ale', 'description', 0, '2019-03-29 22:00:34');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('German-Style Weizenbock', 'description', 0, '2018-05-19 11:19:03');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Belgian-Style Tripel', 'description', 0, '2019-02-25 10:26:05');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Bocks', 'description', 0, '2019-04-28 16:02:25');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('American Brett', 'description', 0, '2018-04-08 04:03:53');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('English-Style Brown Porter', 'description', 0, '2019-01-02 08:52:23');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('German-Style Altbier', 'description', 0, '2018-12-01 03:19:10');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('American Imperial Red Ale', 'description', 0, '2018-06-12 21:28:28');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('German-Style Dunkelweizen', 'description', 0, '2019-10-01 20:03:54');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Wheat Beers', 'description', 0, '2019-09-01 03:38:05');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Wheat Beers', 'description', 0, '2019-04-04 05:38:03');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('American Lager', 'description', 0, '2018-03-25 20:29:11');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('German-Style Kolsch', 'description', 0, '2018-07-28 05:21:50');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Barrel-Aged Beer', 'description', 0, '2019-04-26 02:04:05');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Herb and Spice Beer', 'description', 0, '2019-02-13 21:53:48');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Strong Ales', 'description', 0, '2018-02-08 21:18:40');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('German-Style Hefeweizen', 'description', 0, '2018-05-31 18:31:36');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Specialty Beer', 'description', 0, '2019-01-03 00:57:46');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Vienna-Style Lager', 'description', 0, '2019-12-14 12:44:31');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('English-Style Bitter', 'description', 0, '2019-02-25 02:52:44');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Dark Lagers', 'description', 0, '2018-12-20 06:23:44');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('American Black Ale', 'description', 0, '2018-06-03 10:20:21');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Dark Lagers', 'description', 0, '2018-12-07 14:11:54');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Bocks', 'description', 0, '2018-12-16 12:53:41');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Scotch Ale/Wee Heavy', 'description', 0, '2018-02-09 13:45:11');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('English-Style IPA', 'description', 0, '2019-09-20 13:45:11');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Honey Beer', 'description', 0, '2018-12-21 11:39:00');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Irish-Style Red Beer', 'description', 0, '2019-03-30 03:37:10');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('California Common', 'description', 0, '2019-08-04 16:32:40');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('Scottish-Style Ales', 'description', 0, '2019-11-09 23:23:26');
INSERT INTO beertag.styles (style_name, style_description, is_deleted, date_created) VALUES ('American Imperial Red Ale', 'description', 0, '2018-08-23 01:06:22');

INSERT INTO beertag.beers (beer_name, beer_description, abv, date_added, brewery_id, style_id, beer_picture, is_deleted, average_rating, user_id) VALUES ('Milwaukees Best Ice', 'This is the description', 27.3, '2019-12-14 12:44:31', 28, 57, '/media/uploads/beer-example.jpg', 0, 9, 3);
INSERT INTO beertag.beers (beer_name, beer_description, abv, date_added, brewery_id, style_id, beer_picture, is_deleted, average_rating, user_id) VALUES ('Busch Light', 'This is the description', 33.2, '2018-08-15 06:37:37', 27, 82, '/media/uploads/beer-example.jpg', 0, 8, 3);
INSERT INTO beertag.beers (beer_name, beer_description, abv, date_added, brewery_id, style_id, beer_picture, is_deleted, average_rating, user_id) VALUES ('Dos Equis', 'This is the description', 3.4, '2018-03-04 11:06:17', 30, 1, '/media/uploads/beer-example.jpg', 0, 4, 2);
INSERT INTO beertag.beers (beer_name, beer_description, abv, date_added, brewery_id, style_id, beer_picture, is_deleted, average_rating, user_id) VALUES ('Icehouse', 'This is the description', 12.2, '2018-06-10 07:00:54', 26, 52, '/media/uploads/beer-example.jpg', 0, 5, 4);
INSERT INTO beertag.beers (beer_name, beer_description, abv, date_added, brewery_id, style_id, beer_picture, is_deleted, average_rating, user_id) VALUES ('Keystone Light', 'This is the description', 35.0, '2018-01-21 21:45:17', 21, 55, '/media/uploads/beer-example.jpg', 0, 2, 2);
INSERT INTO beertag.beers (beer_name, beer_description, abv, date_added, brewery_id, style_id, beer_picture, is_deleted, average_rating, user_id) VALUES ('Icehouse', 'This is the description', 39.3, '2019-04-26 02:04:05', 17, 46, '/media/uploads/beer-example.jpg', 0, 1, 3);
INSERT INTO beertag.beers (beer_name, beer_description, abv, date_added, brewery_id, style_id, beer_picture, is_deleted, average_rating, user_id) VALUES ('Keystone Light', 'This is the description', 34.8, '2018-01-08 03:38:09', 21, 2, '/media/uploads/beer-example.jpg', 0, 2, 2);
INSERT INTO beertag.beers (beer_name, beer_description, abv, date_added, brewery_id, style_id, beer_picture, is_deleted, average_rating, user_id) VALUES ('Milwaukees Best Ice', 'This is the description', 27.1, '2019-04-18 05:09:23', 15, 46, '/media/uploads/beer-example.jpg', 0, 8, 2);
INSERT INTO beertag.beers (beer_name, beer_description, abv, date_added, brewery_id, style_id, beer_picture, is_deleted, average_rating, user_id) VALUES ('Corona Extra', 'This is the description', 36.7, '2019-09-05 05:22:08', 30, 83, '/media/uploads/beer-example.jpg', 0, 4, 4);
INSERT INTO beertag.beers (beer_name, beer_description, abv, date_added, brewery_id, style_id, beer_picture, is_deleted, average_rating, user_id) VALUES ('Dos Equis', 'This is the description', 15.0, '2018-01-07 19:20:38', 25, 26, '/media/uploads/beer-example.jpg', 0, 9, 2);
INSERT INTO beertag.beers (beer_name, beer_description, abv, date_added, brewery_id, style_id, beer_picture, is_deleted, average_rating, user_id) VALUES ('Blue Moon', 'This is the description', 35.1, '2018-02-21 19:53:06', 17, 43, '/media/uploads/beer-example.jpg', 0, 4, 2);
INSERT INTO beertag.beers (beer_name, beer_description, abv, date_added, brewery_id, style_id, beer_picture, is_deleted, average_rating, user_id) VALUES ('Milwaukees Best Ice', 'This is the description', 19.6, '2018-12-08 12:34:27', 22, 57, '/media/uploads/beer-example.jpg', 0, 8, 2);
INSERT INTO beertag.beers (beer_name, beer_description, abv, date_added, brewery_id, style_id, beer_picture, is_deleted, average_rating, user_id) VALUES ('Modelo Especial', 'This is the description', 22.7, '2019-01-16 03:04:49', 27, 53, '/media/uploads/beer-example.jpg', 0, 9, 2);
INSERT INTO beertag.beers (beer_name, beer_description, abv, date_added, brewery_id, style_id, beer_picture, is_deleted, average_rating, user_id) VALUES ('Pabst Blue Ribbon', 'This is the description', 13.6, '2019-10-01 20:03:54', 7, 19, '/media/uploads/beer-example.jpg', 0, 4, 2);
INSERT INTO beertag.beers (beer_name, beer_description, abv, date_added, brewery_id, style_id, beer_picture, is_deleted, average_rating, user_id) VALUES ('Keystone Light', 'This is the description', 34.7, '2019-10-01 08:56:21', 24, 24, '/media/uploads/beer-example.jpg', 0, 4, 2);
INSERT INTO beertag.beers (beer_name, beer_description, abv, date_added, brewery_id, style_id, beer_picture, is_deleted, average_rating, user_id) VALUES ('Natural Ice', 'This is the description', 11.8, '2019-10-26 13:33:18', 21, 44, '/media/uploads/beer-example.jpg', 0, 6, 2);
INSERT INTO beertag.beers (beer_name, beer_description, abv, date_added, brewery_id, style_id, beer_picture, is_deleted, average_rating, user_id) VALUES ('Milwaukees Best Light', 'This is the description', 28.7, '2019-10-04 18:57:36', 28, 32, '/media/uploads/beer-example.jpg', 0, 1, 4);
INSERT INTO beertag.beers (beer_name, beer_description, abv, date_added, brewery_id, style_id, beer_picture, is_deleted, average_rating, user_id) VALUES ('Natural Light', 'This is the description', 18.1, '2018-06-18 22:50:23', 7, 77, '/media/uploads/beer-example.jpg', 0, 5, 2);
INSERT INTO beertag.beers (beer_name, beer_description, abv, date_added, brewery_id, style_id, beer_picture, is_deleted, average_rating, user_id) VALUES ('Miller Lite', 'This is the description', 22.7, '2018-01-25 03:32:12', 16, 46, '/media/uploads/beer-example.jpg', 0, 4, 3);
INSERT INTO beertag.beers (beer_name, beer_description, abv, date_added, brewery_id, style_id, beer_picture, is_deleted, average_rating, user_id) VALUES ('Corona Light', 'This is the description', 32.4, '2019-06-06 09:22:20', 17, 46, '/media/uploads/beer-example.jpg', 0, 1, 2);
INSERT INTO beertag.beers (beer_name, beer_description, abv, date_added, brewery_id, style_id, beer_picture, is_deleted, average_rating, user_id) VALUES ('Keystone Light', 'This is the description', 22.1, '2018-07-27 09:43:19', 16, 88, '/media/uploads/beer-example.jpg', 0, 4, 3);
INSERT INTO beertag.beers (beer_name, beer_description, abv, date_added, brewery_id, style_id, beer_picture, is_deleted, average_rating, user_id) VALUES ('Tecate', 'This is the description', 9.4, '2018-01-20 15:13:49', 27, 2, '/media/uploads/beer-example.jpg', 0, 1, 3);
INSERT INTO beertag.beers (beer_name, beer_description, abv, date_added, brewery_id, style_id, beer_picture, is_deleted, average_rating, user_id) VALUES ('Milwaukees Best Ice', 'This is the description', 32.1, '2019-09-05 05:22:08', 27, 43, '/media/uploads/beer-example.jpg', 0, 1, 3);
INSERT INTO beertag.beers (beer_name, beer_description, abv, date_added, brewery_id, style_id, beer_picture, is_deleted, average_rating, user_id) VALUES ('Icehouse', 'This is the description', 23.3, '2019-04-26 15:22:21', 21, 88, '/media/uploads/beer-example.jpg', 0, 6, 2);
INSERT INTO beertag.beers (beer_name, beer_description, abv, date_added, brewery_id, style_id, beer_picture, is_deleted, average_rating, user_id) VALUES ('Blue Moon', 'This is the description', 30.2, '2018-04-03 04:25:36', 22, 69, '/media/uploads/beer-example.jpg', 0, 2, 3);
INSERT INTO beertag.beers (beer_name, beer_description, abv, date_added, brewery_id, style_id, beer_picture, is_deleted, average_rating, user_id) VALUES ('Natural Light', 'This is the description', 28.2, '2019-11-12 13:54:41', 7, 80, '/media/uploads/beer-example.jpg', 0, 2, 3);

INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 23, 8, 'Stava', '2019-04-26 21:29:37');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 9, 6, 'Top e', '2018-04-08 04:03:53');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 18, 9, 'Mnogo mi haresa', '2019-10-30 01:03:06');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 18, 5, 'Mnogo mi haresa', '2018-04-29 17:32:01');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 11, 5, 'Mnogo mi haresa', '2019-10-11 02:01:52');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 10, 5, 'Ima i po-hubavi', '2018-02-27 08:20:18');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 18, 7, 'Top e', '2019-06-01 19:37:28');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 23, 6, 'Liubimata mi bira', '2018-01-20 15:13:49');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 18, 6, 'Ima i po-hubavi', '2019-04-26 15:22:21');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 12, 7, 'Ne mi haresa tolkova', '2018-06-03 10:20:21');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 19, 5, 'Liubimata mi bira', '2018-02-03 10:34:28');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 26, 6, 'Evalarka', '2019-10-26 17:24:57');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 18, 6, 'Stava', '2018-09-10 11:16:14');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 16, 2, 'Liubimata mi bira', '2019-10-06 18:30:54');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 1, 9, 'Mnogo mi haresa', '2019-02-14 06:09:13');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 23, 5, 'Ima i po-hubavi', '2018-12-16 12:53:41');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 9, 7, 'Stava', '2019-01-03 00:57:46');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 25, 6, 'Ne mi haresa tolkova', '2019-10-27 00:46:44');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 8, 7, 'Mnogo mi haresa', '2018-01-25 13:12:12');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 1, 6, 'Mnogo mi haresa', '2019-10-26 13:33:18');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 26, 4, 'Mnogo mi haresa', '2019-08-05 06:36:49');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 23, 8, 'Mnogo mi haresa', '2018-10-27 16:16:47');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 16, 2, 'Ima i po-hubavi', '2019-09-05 05:22:08');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 26, 7, 'Ne mi haresa tolkova', '2019-07-01 15:59:38');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 23, 9, 'Evalarka', '2018-04-08 04:03:53');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 15, 9, 'Top e', '2019-05-19 02:52:52');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 2, 7, 'Stava', '2019-03-05 10:33:30');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 23, 5, 'Ne mi haresa tolkova', '2019-05-01 12:07:33');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 11, 5, 'Evalarka', '2019-03-17 19:57:24');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 23, 5, 'Liubimata mi bira', '2018-06-03 10:20:21');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 19, 6, 'Ne mi haresa tolkova', '2018-12-01 03:19:10');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 18, 6, 'Liubimata mi bira', '2018-04-29 17:32:01');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 10, 4, 'Stava', '2018-10-02 16:45:41');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 25, 6, 'Liubimata mi bira', '2018-10-02 16:45:41');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 1, 9, 'Mnogo mi haresa', '2019-07-28 06:16:37');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 17, 2, 'Evalarka', '2018-09-11 22:28:12');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 23, 7, 'Ne mi haresa tolkova', '2019-08-06 03:35:26');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 25, 5, 'Ne mi haresa tolkova', '2018-06-06 07:57:29');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 7, 7, 'Liubimata mi bira', '2018-10-14 09:26:53');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 22, 7, 'Liubimata mi bira', '2019-09-01 03:38:05');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 2, 9, 'Liubimata mi bira', '2019-05-24 23:41:27');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 10, 5, 'Mnogo mi haresa', '2018-02-21 19:53:06');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 18, 7, 'Evalarka', '2019-03-26 07:20:31');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 10, 7, 'Liubimata mi bira', '2019-08-13 11:08:25');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 23, 4, 'Evalarka', '2018-10-02 16:45:41');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 26, 7, 'Ne mi haresa tolkova', '2018-01-17 02:22:42');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 10, 8, 'Liubimata mi bira', '2018-12-08 12:34:27');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 17, 8, 'Stava', '2018-07-28 05:21:50');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 18, 7, 'Ima i po-hubavi', '2018-09-11 22:28:12');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 16, 2, 'Ne mi haresa tolkova', '2019-10-10 00:13:08');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 19, 9, 'Stava', '2018-11-18 01:24:33');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 10, 6, 'Ne mi haresa tolkova', '2018-01-25 13:12:12');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 18, 4, 'Top e', '2018-09-11 22:28:12');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 11, 6, 'Evalarka', '2018-12-15 20:41:52');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 12, 2, 'Evalarka', '2019-10-10 00:13:08');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 9, 8, 'Top e', '2019-05-16 20:50:19');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 25, 6, 'Stava', '2019-05-25 21:42:57');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 17, 5, 'Ne mi haresa tolkova', '2018-11-18 01:24:33');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 22, 5, 'Mnogo mi haresa', '2018-09-27 09:47:20');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 2, 5, 'Ne mi haresa tolkova', '2018-04-22 05:46:52');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 23, 6, 'Top e', '2019-09-07 22:20:20');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 23, 4, 'Evalarka', '2018-02-14 20:45:39');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 7, 6, 'Ima i po-hubavi', '2018-06-04 16:58:32');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 18, 6, 'Top e', '2018-07-21 08:54:41');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 1, 2, 'Mnogo mi haresa', '2019-02-25 02:52:44');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 26, 5, 'Mnogo mi haresa', '2018-06-04 16:58:32');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 12, 5, 'Evalarka', '2018-12-07 14:11:54');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 23, 7, 'Liubimata mi bira', '2019-03-26 07:20:31');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 17, 6, 'Mnogo mi haresa', '2018-03-07 20:31:42');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 7, 4, 'Evalarka', '2018-06-20 09:51:52');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 10, 8, 'Mnogo mi haresa', '2019-03-05 10:33:30');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 11, 2, 'Top e', '2019-10-01 20:03:54');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 17, 5, 'Liubimata mi bira', '2019-10-01 08:56:21');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 1, 6, 'Stava', '2019-02-26 06:45:19');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 1, 7, 'Liubimata mi bira', '2018-12-25 09:20:48');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 18, 6, 'Evalarka', '2018-07-27 09:43:19');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 11, 6, 'Evalarka', '2018-08-15 06:37:37');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 1, 5, 'Ima i po-hubavi', '2018-03-19 22:57:03');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 2, 8, 'Mnogo mi haresa', '2018-09-10 11:16:14');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 18, 8, 'Ne mi haresa tolkova', '2018-02-09 05:53:25');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 23, 6, 'Ima i po-hubavi', '2019-03-17 19:57:24');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 23, 6, 'Stava', '2019-08-25 13:28:22');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 26, 2, 'Liubimata mi bira', '2019-03-17 03:37:40');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 18, 5, 'Stava', '2019-08-25 13:28:22');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 10, 2, 'Top e', '2019-11-24 05:08:44');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 17, 2, 'Top e', '2019-01-14 17:59:48');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 23, 9, 'Ima i po-hubavi', '2019-10-10 00:13:08');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 9, 2, 'Top e', '2019-10-04 18:57:36');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 16, 4, 'Liubimata mi bira', '2018-04-13 13:07:42');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 16, 9, 'Stava', '2018-02-14 20:45:39');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 18, 7, 'Top e', '2019-04-11 15:10:27');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 2, 7, 'Evalarka', '2018-03-18 14:50:58');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 16, 7, 'Ne mi haresa tolkova', '2019-10-10 00:13:08');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 18, 5, 'Ima i po-hubavi', '2018-12-25 09:20:48');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 25, 2, 'Liubimata mi bira', '2019-01-02 18:57:12');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 23, 5, 'Liubimata mi bira', '2018-02-14 20:45:39');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 18, 7, 'Stava', '2019-05-29 12:42:10');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 18, 2, 'Ne mi haresa tolkova', '2019-12-13 22:20:09');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 10, 5, 'Stava', '2019-01-25 00:16:29');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 23, 8, 'Mnogo mi haresa', '2019-02-23 15:33:31');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 8, 7, 'Top e', '2018-02-21 19:53:06');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 23, 8, 'Ima i po-hubavi', '2018-07-02 02:29:13');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 18, 4, 'Top e', '2019-08-15 21:22:20');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 16, 2, 'Stava', '2019-05-29 12:42:10');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 23, 7, 'Ima i po-hubavi', '2019-08-20 02:19:42');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 10, 7, 'Ima i po-hubavi', '2018-04-29 17:32:01');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 18, 7, 'Evalarka', '2019-08-13 11:08:25');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 8, 2, 'Liubimata mi bira', '2018-04-12 16:05:47');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 10, 5, 'Ima i po-hubavi', '2019-09-07 22:20:20');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 16, 2, 'Top e', '2019-04-26 02:04:05');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 23, 8, 'Ima i po-hubavi', '2019-11-15 04:02:19');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 18, 6, 'Top e', '2018-12-21 11:39:00');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 15, 2, 'Ima i po-hubavi', '2019-05-23 21:35:21');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 23, 7, 'Liubimata mi bira', '2018-10-04 00:37:17');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 1, 9, 'Liubimata mi bira', '2018-05-31 18:31:36');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 18, 6, 'Top e', '2019-11-15 04:02:19');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 10, 5, 'Mnogo mi haresa', '2018-07-02 09:31:45');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 1, 8, 'Evalarka', '2019-05-30 22:41:52');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 16, 4, 'Ima i po-hubavi', '2019-03-03 15:54:52');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 7, 9, 'Mnogo mi haresa', '2018-10-04 00:37:17');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 15, 2, 'Ne mi haresa tolkova', '2018-05-19 17:24:18');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 23, 4, 'Ne mi haresa tolkova', '2018-10-24 04:58:17');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 18, 6, 'Liubimata mi bira', '2018-10-02 16:45:41');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 9, 6, 'Stava', '2019-05-24 23:41:27');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 11, 5, 'Stava', '2019-04-26 02:20:34');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 9, 6, 'Evalarka', '2019-04-11 15:10:27');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 16, 8, 'Ima i po-hubavi', '2019-02-25 02:52:44');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 9, 4, 'Top e', '2018-03-25 20:29:11');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 10, 6, 'Stava', '2019-10-07 15:49:19');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 12, 7, 'Evalarka', '2018-09-10 11:16:14');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 18, 8, 'Evalarka', '2018-06-10 07:00:54');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 12, 9, 'Stava', '2019-12-14 12:44:31');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 2, 6, 'Ne mi haresa tolkova', '2018-06-03 10:20:21');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 11, 8, 'Mnogo mi haresa', '2018-05-16 17:57:50');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 10, 6, 'Evalarka', '2018-03-19 22:57:03');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 9, 4, 'Mnogo mi haresa', '2019-11-14 02:51:18');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 10, 5, 'Ima i po-hubavi', '2018-02-21 19:53:06');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 26, 6, 'Ne mi haresa tolkova', '2018-05-17 09:03:39');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 9, 4, 'Stava', '2018-02-12 04:20:19');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 26, 6, 'Mnogo mi haresa', '2019-07-10 20:23:17');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 10, 6, 'Ne mi haresa tolkova', '2019-03-03 15:54:52');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 25, 5, 'Evalarka', '2018-07-28 05:21:50');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 23, 5, 'Top e', '2019-09-29 21:50:52');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 9, 9, 'Stava', '2019-09-15 09:43:38');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 23, 5, 'Ne mi haresa tolkova', '2018-05-19 17:24:18');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 16, 8, 'Ima i po-hubavi', '2018-04-29 17:32:01');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 11, 7, 'Top e', '2019-07-23 05:42:45');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 10, 8, 'Mnogo mi haresa', '2018-02-08 21:18:40');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 10, 8, 'Ima i po-hubavi', '2018-09-05 14:01:00');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 15, 5, 'Mnogo mi haresa', '2018-12-25 09:20:48');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 23, 9, 'Liubimata mi bira', '2019-10-18 23:05:36');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 19, 5, 'Top e', '2018-10-02 16:45:41');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 10, 6, 'Mnogo mi haresa', '2019-08-04 16:32:40');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 23, 4, 'Stava', '2018-10-05 21:07:54');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 1, 8, 'Liubimata mi bira', '2019-12-02 05:38:27');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 9, 7, 'Liubimata mi bira', '2019-12-13 22:20:09');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 23, 5, 'Mnogo mi haresa', '2018-12-30 11:38:10');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 18, 8, 'Evalarka', '2018-02-09 13:45:11');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 25, 8, 'Mnogo mi haresa', '2019-10-07 20:13:58');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 11, 6, 'Ne mi haresa tolkova', '2018-05-19 11:19:03');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 16, 5, 'Ima i po-hubavi', '2019-07-10 20:23:17');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 10, 6, 'Ne mi haresa tolkova', '2019-09-07 22:20:20');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 26, 6, 'Evalarka', '2019-05-21 03:51:17');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 11, 8, 'Liubimata mi bira', '2018-04-29 17:32:01');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 26, 5, 'Liubimata mi bira', '2019-03-26 09:48:52');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 23, 9, 'Evalarka', '2019-06-28 09:11:08');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 15, 6, 'Mnogo mi haresa', '2018-12-21 11:39:00');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 9, 9, 'Mnogo mi haresa', '2018-07-02 02:29:13');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 2, 6, 'Mnogo mi haresa', '2019-11-14 02:51:18');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 11, 9, 'Mnogo mi haresa', '2018-10-02 16:45:41');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 11, 7, 'Ne mi haresa tolkova', '2018-12-08 12:34:27');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 17, 5, 'Evalarka', '2018-12-08 12:34:27');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 2, 6, 'Top e', '2019-08-11 06:12:45');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 9, 5, 'Top e', '2018-06-11 13:03:36');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 23, 2, 'Mnogo mi haresa', '2019-10-27 00:46:44');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 1, 6, 'Ima i po-hubavi', '2019-02-23 07:34:42');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 18, 5, 'Liubimata mi bira', '2019-12-14 12:44:31');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 19, 2, 'Evalarka', '2019-09-19 08:59:09');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 18, 7, 'Evalarka', '2018-08-29 18:00:33');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 16, 4, 'Liubimata mi bira', '2019-06-06 09:22:20');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 12, 5, 'Liubimata mi bira', '2019-03-17 03:37:40');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 17, 6, 'Ne mi haresa tolkova', '2018-07-03 03:21:42');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 10, 6, 'Ne mi haresa tolkova', '2019-10-26 13:33:18');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 17, 7, 'Mnogo mi haresa', '2018-04-03 04:25:36');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 22, 4, 'Stava', '2018-06-10 07:00:54');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 16, 4, 'Liubimata mi bira', '2019-08-06 03:35:26');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 15, 2, 'Ima i po-hubavi', '2018-05-21 12:51:09');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 8, 8, 'Evalarka', '2018-02-21 19:53:06');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 16, 5, 'Evalarka', '2019-10-26 17:24:57');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 23, 8, 'Ne mi haresa tolkova', '2019-04-11 15:10:27');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 1, 6, 'Evalarka', '2018-05-02 00:02:02');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 23, 2, 'Ne mi haresa tolkova', '2018-11-13 22:14:44');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 23, 4, 'Mnogo mi haresa', '2019-10-26 13:33:18');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 8, 9, 'Ne mi haresa tolkova', '2018-03-31 01:17:26');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 8, 9, 'Top e', '2019-09-06 23:16:47');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 18, 2, 'Mnogo mi haresa', '2018-05-19 02:39:44');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 7, 2, 'Evalarka', '2019-05-26 10:48:46');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 8, 4, 'Liubimata mi bira', '2018-05-18 16:56:23');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 17, 6, 'Ima i po-hubavi', '2019-01-16 03:04:49');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 18, 4, 'Evalarka', '2018-03-04 11:06:17');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 18, 9, 'Evalarka', '2018-10-24 04:58:17');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 15, 5, 'Top e', '2018-05-19 11:19:03');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 9, 4, 'Ne mi haresa tolkova', '2018-10-04 00:37:17');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 26, 6, 'Liubimata mi bira', '2019-03-17 03:37:40');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 23, 8, 'Evalarka', '2019-09-05 05:22:08');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 1, 6, 'Top e', '2019-08-25 13:28:22');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 17, 8, 'Mnogo mi haresa', '2019-02-13 21:53:48');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 9, 5, 'Evalarka', '2019-02-26 06:45:19');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 26, 5, 'Mnogo mi haresa', '2018-02-21 19:53:06');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 1, 5, 'Mnogo mi haresa', '2018-01-17 02:22:42');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 16, 9, 'Stava', '2019-12-02 05:38:27');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 10, 5, 'Ima i po-hubavi', '2019-12-02 05:38:27');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 10, 9, 'Liubimata mi bira', '2019-04-26 01:30:01');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 18, 4, 'Liubimata mi bira', '2018-10-26 10:29:56');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 17, 4, 'Stava', '2018-12-30 11:38:10');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 2, 6, 'Stava', '2019-02-25 02:52:44');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 17, 9, 'Top e', '2019-07-10 20:23:17');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 25, 6, 'Mnogo mi haresa', '2019-11-15 04:02:19');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 18, 6, 'Top e', '2018-03-31 01:17:26');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 7, 6, 'Stava', '2019-09-19 08:59:09');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 17, 8, 'Mnogo mi haresa', '2018-04-24 14:52:27');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 1, 4, 'Mnogo mi haresa', '2018-10-02 16:45:41');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 18, 5, 'Mnogo mi haresa', '2018-02-25 17:25:53');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 9, 5, 'Top e', '2018-10-02 11:45:53');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 25, 9, 'Stava', '2018-11-18 01:24:33');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 18, 8, 'Top e', '2018-06-03 10:20:21');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 25, 5, 'Top e', '2018-12-08 12:34:27');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 11, 2, 'Ne mi haresa tolkova', '2019-05-01 12:07:33');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 18, 5, 'Mnogo mi haresa', '2019-12-14 12:44:31');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 1, 6, 'Stava', '2019-04-03 10:18:25');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 18, 4, 'Stava', '2019-02-25 10:26:05');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 23, 6, 'Top e', '2019-04-26 15:22:21');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 15, 6, 'Top e', '2018-07-04 12:29:06');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 10, 2, 'Top e', '2019-07-10 20:23:17');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 17, 8, 'Evalarka', '2019-02-14 12:53:58');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 23, 4, 'Ima i po-hubavi', '2019-11-24 05:08:44');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 18, 6, 'Stava', '2019-09-07 22:20:20');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 10, 7, 'Liubimata mi bira', '2019-01-25 00:16:29');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 2, 2, 'Ima i po-hubavi', '2019-10-27 00:46:44');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 18, 8, 'Ima i po-hubavi', '2018-09-27 05:25:44');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 22, 7, 'Evalarka', '2019-03-26 09:48:52');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 23, 6, 'Stava', '2018-11-14 12:34:46');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 16, 6, 'Evalarka', '2018-01-25 13:12:12');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 10, 5, 'Top e', '2018-04-13 13:07:42');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 25, 9, 'Liubimata mi bira', '2018-03-18 14:50:58');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 12, 2, 'Ima i po-hubavi', '2019-04-06 03:55:44');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 12, 2, 'Stava', '2018-05-23 11:42:54');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 9, 6, 'Mnogo mi haresa', '2019-11-24 05:08:44');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 18, 5, 'Evalarka', '2019-04-26 01:30:01');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 18, 6, 'Mnogo mi haresa', '2019-09-07 22:20:20');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 7, 9, 'Liubimata mi bira', '2019-03-03 15:54:52');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 25, 8, 'Top e', '2018-08-15 06:37:37');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 16, 2, 'Ne mi haresa tolkova', '2018-05-17 09:03:39');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 18, 8, 'Liubimata mi bira', '2018-06-12 21:28:28');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 9, 4, 'Ima i po-hubavi', '2018-12-01 03:19:10');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 1, 8, 'Ne mi haresa tolkova', '2018-05-31 18:31:36');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 9, 7, 'Liubimata mi bira', '2019-02-23 07:34:42');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 22, 6, 'Top e', '2018-10-26 10:29:56');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 16, 9, 'Ima i po-hubavi', '2019-01-03 00:57:46');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 23, 9, 'Liubimata mi bira', '2018-06-11 13:03:36');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 7, 8, 'Liubimata mi bira', '2018-03-25 20:29:11');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 16, 6, 'Liubimata mi bira', '2019-06-29 01:05:21');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 10, 4, 'Ima i po-hubavi', '2018-05-19 17:24:18');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 23, 6, 'Evalarka', '2019-03-17 19:57:24');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 18, 5, 'Mnogo mi haresa', '2019-09-01 03:38:05');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 18, 6, 'Mnogo mi haresa', '2018-05-23 11:42:54');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 15, 5, 'Liubimata mi bira', '2019-01-21 14:46:53');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 18, 5, 'Mnogo mi haresa', '2019-01-22 16:07:28');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 16, 2, 'Ima i po-hubavi', '2019-01-16 03:04:49');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 7, 8, 'Ima i po-hubavi', '2019-08-26 20:09:22');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 2, 6, 'Stava', '2018-04-29 17:32:01');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 10, 8, 'Liubimata mi bira', '2019-11-12 13:54:41');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 12, 6, 'Evalarka', '2019-01-16 03:04:49');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 1, 4, 'Mnogo mi haresa', '2019-08-06 03:35:26');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 10, 5, 'Liubimata mi bira', '2018-09-27 09:47:20');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 7, 7, 'Stava', '2018-04-02 16:50:12');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 16, 8, 'Top e', '2018-01-20 15:13:49');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 9, 2, 'Mnogo mi haresa', '2019-03-17 09:01:25');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 9, 6, 'Liubimata mi bira', '2018-01-08 03:38:09');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 12, 6, 'Ne mi haresa tolkova', '2019-01-21 14:46:53');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 7, 7, 'Ima i po-hubavi', '2018-08-23 01:06:22');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 23, 9, 'Ne mi haresa tolkova', '2018-04-22 05:46:52');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 1, 9, 'Liubimata mi bira', '2018-06-03 10:20:21');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 1, 5, 'Evalarka', '2018-07-28 05:21:50');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 7, 4, 'Ne mi haresa tolkova', '2018-12-18 11:30:33');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 8, 6, 'Ne mi haresa tolkova', '2018-05-02 00:02:02');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 23, 6, 'Ima i po-hubavi', '2018-12-07 14:11:54');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 23, 9, 'Ima i po-hubavi', '2019-04-26 02:20:34');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 10, 6, 'Liubimata mi bira', '2018-11-14 12:34:46');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 18, 4, 'Stava', '2018-02-25 17:25:53');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 1, 5, 'Ima i po-hubavi', '2019-04-05 07:23:00');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 7, 9, 'Evalarka', '2019-04-05 18:51:14');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 26, 9, 'Mnogo mi haresa', '2018-06-11 13:03:36');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 11, 2, 'Ne mi haresa tolkova', '2019-06-06 09:22:20');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 1, 2, 'Ne mi haresa tolkova', '2018-06-06 07:57:29');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 18, 4, 'Mnogo mi haresa', '2018-07-28 05:21:50');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (3, 18, 9, 'Ima i po-hubavi', '2018-01-20 15:13:49');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (2, 22, 4, 'Top e', '2019-10-01 20:03:54');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 22, 7, 'Top e', '2019-02-23 07:34:42');
INSERT INTO beertag.ratings (user_id, beer_id, rating, review, date_created) VALUES (6, 8, 7, 'Mnogo mi haresa', '2019-12-14 12:44:31');
