create table countries
(
    country_id   int auto_increment
        primary key,
    country_name varchar(255)         not null,
    is_deleted   tinyint(1) default 0 null,
    constraint countries_country_name_uindex
        unique (country_name)
);
create table breweries
(
    brewery_id      int auto_increment
        primary key,
    brewery_name    varchar(255)         not null,
    date_created    datetime             not null,
    country_id      int                  not null comment 'This field shows the origin country of the brewery',
    brewery_picture blob                 null,
    is_deleted      tinyint(1) default 0 null,
    constraint breweries_countries_fk
        foreign key (country_id) references countries (country_id)
);
create table status
(
    status_id   int          not null
        primary key,
    status_name varchar(255) not null
);
create table styles
(
    style_id          int auto_increment
        primary key,
    style_name        varchar(255)         not null,
    style_description varchar(255)         null,
    is_deleted        tinyint(1) default 0 null,
    date_created      datetime             null
);
create table tags
(
    tag_id       int auto_increment
        primary key,
    tag_name     varchar(255)         not null,
    date_created datetime             not null,
    is_deleted   tinyint(1) default 0 null
);
create table users
(
    username varchar(50) not null
        primary key,
    password varchar(68) not null,
    enabled  tinyint     not null
);
create table authorities
(
    username  varchar(50) not null,
    authority varchar(50) not null,
    constraint username_authority
        unique (username, authority),
    constraint FK__users
        foreign key (username) references users (username)
);
create table users_details
(
    user_id         int auto_increment
        primary key,
    first_name      varchar(255) null,
    last_name       varchar(255) null,
    email           varchar(255) not null,
    country_id      int          null comment 'This field shows the user''s country.',
    profile_picture varchar(200) null,
    login_username  varchar(50)  null,
    constraint users_email_uindex
        unique (email),
    constraint users_countries_fk
        foreign key (country_id) references countries (country_id),
    constraint users_details_users_username_fk
        foreign key (login_username) references users (username)
);
create table beers
(
    beer_id          int auto_increment
        primary key,
    beer_name        varchar(255)         not null,
    beer_description text                 null,
    abv              double               not null,
    date_added       datetime             not null,
    brewery_id       int                  not null comment 'This field shows the brewery where the beer was made.',
    style_id         int                  not null comment 'This field shows the beer style.',
    beer_picture     blob                 null,
    is_deleted       tinyint(1) default 0 null,
    average_rating   int                  null,
    user_id          int                  null,
    constraint beers_breweries_fk
        foreign key (brewery_id) references breweries (brewery_id),
    constraint beers_styles_fk
        foreign key (style_id) references styles (style_id),
    constraint beers_users_user_id_fk
        foreign key (user_id) references users_details (user_id)
);
create table beer_tags
(
    beer_id int null,
    tag_id  int null,
    constraint beer_tags_beers_id_fk
        foreign key (beer_id) references beers (beer_id),
    constraint beer_tags_tags_id_fk
        foreign key (tag_id) references tags (tag_id)
);
create table ratings
(
    id_rating    int auto_increment
        primary key,
    user_id      int          null,
    beer_id      int          null,
    rating       int          not null,
    review       varchar(255) null,
    date_created datetime     null,
    constraint ratings_beers_beer_id_fk
        foreign key (beer_id) references beers (beer_id),
    constraint ratings_users_user_id_fk
        foreign key (user_id) references users_details (user_id)
);
create table users_beers_status
(
    users_beers_status_id int auto_increment
        primary key,
    beer_id               int null,
    status_id             int null,
    user_id               int null,
    constraint beers_status_status_status_id_fk
        foreign key (status_id) references status (status_id),
    constraint users_beers_beers_id_fk
        foreign key (beer_id) references beers (beer_id),
    constraint users_beers_users_id_fk
        foreign key (user_id) references users_details (user_id)
);